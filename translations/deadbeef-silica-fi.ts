<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/dialogs/About.qml" line="27"/>
        <source>About</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="42"/>
        <source>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 This program is licensed under the terms of GPLv3 license
                                 
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues                                  tracker to file bug reports and feature requests.
                                 
                                 Credits:
                                 
                                 %3</source>
        <translation>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 Tämä ohjelma on lisensoitu GPLv3-lisenssin mukaisesti
                                 
                                 Käytäthän https://bitbucket.org/kravich/deadbeef-silica/issues                                  seurantatyökalua ilmoittaaksesi virheistä ja toivoaksesi ominasuuksia.
                                 
                                 Kiitokset:
                                 
                                 %3</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="53"/>
        <source>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Russian translation</source>
        <translation>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Venäjänkielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="59"/>
        <source>Allan Nordhøy
                                                    Norwegian Bokmål translation</source>
        <translation>Allan Nordhøy
                                                    Kirjanorjalainen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="67"/>
        <source>Rui Kon
                                                    Elizabeth Sherrock
                                                    Chinese translation</source>
        <translation>Rui Kon
                                                    Elizabeth Sherrock
                                                    Kiinankielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="70"/>
        <source>Szabó G
                                                    Hungarian translation</source>
        <translation>Szabó G
                                                    Unkarinkielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="56"/>
        <source>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Spanish translation</source>
        <translation>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Espanjankielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="61"/>
        <source>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    French translation</source>
        <translation>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    Ranskankielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="72"/>
        <source>O S
                                                    J. Lavoie
                                                    German translation</source>
        <translation>O S
                                                    J. Lavoie
                                                    Saksankielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="75"/>
        <source>Åke Engelbrektson
                                                     Swedish translation</source>
        <translation>Åke Engelbrektson
                                                     Ruotsinkielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="77"/>
        <source>Reima Vesterinen
                                                     Finnish translation</source>
        <translation>Reima Vesterinen
                                                     Suomenkielinen käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="79"/>
        <source>Milo Ivir
                                                     Croatian translation</source>
        <translation>Milo Ivir
                                                     Kroatian käännös</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="120"/>
        <source>DeadBeef License</source>
        <translation>DeadBeef Lisenssi</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="131"/>
        <source>DeadBeef is licensed under the terms of zlib license. For more information please refer to %1</source>
        <translation>DeadBeef on lisensoitu zlib-lisenssin mukaisesti. Lisätietoja varten katso %1</translation>
    </message>
</context>
<context>
    <name>AddLocation</name>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="21"/>
        <source>Add location</source>
        <translation>Lisää lähde</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="41"/>
        <source>Location (URI or file path)</source>
        <translation>Lähde (URI tai tiedostosijainti)</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>kHz</source>
        <translation>kHz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="20"/>
        <source>Zero all</source>
        <translation>Nollaa kaikki</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="47"/>
        <source>Equalizer</source>
        <translation>Taajuuskorjain</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="59"/>
        <source>Enable</source>
        <translation>Ota käyttöön</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="109"/>
        <source>Preamp</source>
        <translation>Esivahvistin</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>+%1 dB</source>
        <translation>+%1 dB</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="55"/>
        <source>Hide system files</source>
        <translation>Piilota järjestelmätiedostot</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="56"/>
        <source>Show system files</source>
        <translation>Näytä järjestelmätiedostot</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="65"/>
        <source>Select current dir</source>
        <translation>Valitse nykyinen kansio</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="66"/>
        <source>Select</source>
        <translation>Valitse</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="84"/>
        <source>Filter</source>
        <translation>Suodatin</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="108"/>
        <source>Path</source>
        <translation>Polku</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="13"/>
        <source>untitled</source>
        <comment>default name of the file to be saved</comment>
        <translation>nimetön</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="106"/>
        <source>Location</source>
        <translation>Sijainti</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="130"/>
        <source>Select file location</source>
        <translation>Valitse tiedostosijainti</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="145"/>
        <source>New file name</source>
        <translation>Uusi tiedostonimi</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="163"/>
        <source>File exists. Overwrite?</source>
        <translation>Tiedosto on olemassa. Päällekirjoitetaanko?</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="178"/>
        <source>There is already a directory with such name</source>
        <translation>Samanniminen kansio on jo olemassa</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="194"/>
        <source>Format</source>
        <translation>Tiedostomuoto</translation>
    </message>
</context>
<context>
    <name>FileSelectionField</name>
    <message>
        <location filename="../qml/items/FileSelectionField.qml" line="39"/>
        <source>Select %1</source>
        <comment>title of dialog that selects path to file in plugin settings</comment>
        <translation>Valitse %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <source>Supported formats</source>
        <translation>Tuetut tiedostomuodot</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <source>All files</source>
        <translation>Kaikki tiedostot</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>Supported playlist formats</source>
        <translation>Tuetut soittolista muodot</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Open file(s)/folder(s)</source>
        <comment>item of pull-up menu</comment>
        <translation>Avaa tiedosto(ja)/kansio(ita)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Open file(s)/folder(s)</source>
        <comment>title of dialog page</comment>
        <translation>Avaa tiedosto(ja)/kansio(ita)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="105"/>
        <source>Load playlist</source>
        <comment>item of pull-up menu</comment>
        <translation>Lataa soittolista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="109"/>
        <source>Load playlist</source>
        <comment>title of dialog page</comment>
        <translation>Lataa soittolista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>More…</source>
        <translation>Lisää…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>Empty playlist</source>
        <translation>Tyhjä soitolista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="137"/>
        <source>Pull down to add files</source>
        <translation>Vedä alas lisätäksesi tiedostoja</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="203"/>
        <source>Add To Playback Queue</source>
        <translation>Lisää toistojonoon</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="208"/>
        <source>Remove From Playback Queue</source>
        <translation>Poista toistojonosta</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="216"/>
        <source>Track Properties</source>
        <translation>Kappaleen ominaisuudet</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="222"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="229"/>
        <source>Deleting</source>
        <translation>Poistetaan</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>Equalizer</source>
        <translation>Taajuuskorjain</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="272"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="277"/>
        <source>About</source>
        <translation>Tietoja</translation>
    </message>
</context>
<context>
    <name>PlaylistActions</name>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="30"/>
        <source>Supported formats</source>
        <translation>Tuetut tiedostomuodot</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="31"/>
        <source>All files</source>
        <translation>Kaikki tiedostot</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="70"/>
        <source>Playlist actions</source>
        <translation>Soittolistan toiminnot</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="77"/>
        <source>Add file(s)/folder(s)</source>
        <comment>playlist actions menu item</comment>
        <translation>Lisää tiedosto(ja)/kansio(ita)</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="79"/>
        <source>Save playlist</source>
        <comment>playlist actions menu item</comment>
        <translation>Tallenna soittolista</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="90"/>
        <source>Add file(s)/folder(s)</source>
        <comment>file add dialog title</comment>
        <translation>Lisää tiedosto(ja)/kansio(ita)</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="108"/>
        <source>Save playlist</source>
        <comment>save playlist dialog title</comment>
        <translation>Tallenna soittolista</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="78"/>
        <source>Add location</source>
        <translation>Lisää lähde</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="80"/>
        <source>Sort playlist</source>
        <translation>Järjestä soittolista</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="81"/>
        <source>Clear playlist</source>
        <translation>Tyhjennä soittolista</translation>
    </message>
</context>
<context>
    <name>PlaylistNameEdit</name>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Add playlist</source>
        <translation>Lisää soittolista</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Rename playlist</source>
        <translation>Uudelleenimeä soittolista</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="42"/>
        <source>Playlist name</source>
        <translation>Soittolistan nimi</translation>
    </message>
</context>
<context>
    <name>PlaylistSorting</name>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="16"/>
        <source>Sort by</source>
        <translation>Järjestä</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="29"/>
        <source>Reverse order</source>
        <translation>Käänteinen järjestys</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="45"/>
        <source>Artist/Album/Track number</source>
        <translation>Artisti/albumi/kappalenumero</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="46"/>
        <source>Artist/Album</source>
        <translation>Artisti/albumi</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="47"/>
        <source>Title</source>
        <translation>Otsikko</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="48"/>
        <source>Track number</source>
        <translation>Kappalenumero</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="49"/>
        <source>Album</source>
        <translation>Albumi</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="50"/>
        <source>Artist</source>
        <translation>Artisti</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="51"/>
        <source>Album artist</source>
        <translation>Albumin artisti</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="52"/>
        <source>Date</source>
        <translation>Päivämäärä</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="53"/>
        <source>Filename</source>
        <translation>Tiedostonimi</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="40"/>
        <source>New playlist</source>
        <translation>Uusi soittolista</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="19"/>
        <source>Add new playlist</source>
        <translation>Lisää uusi soittolista</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="67"/>
        <source>Playlists</source>
        <translation>Soittolistat</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="94"/>
        <source>Rename</source>
        <translation>Uudelleenimeä</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="114"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="121"/>
        <source>Deleting</source>
        <translation>Poistetaan</translation>
    </message>
</context>
<context>
    <name>PluginCopyright</name>
    <message>
        <location filename="../qml/dialogs/PluginCopyright.qml" line="24"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="29"/>
        <source>Description</source>
        <translation>Kuvaus</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="53"/>
        <source>Version</source>
        <translation>Versio</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="59"/>
        <source>Website</source>
        <translation>Verkkosivut</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="82"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="90"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="31"/>
        <source>plays aac files, supports raw aac files, as well as mp4 container</source>
        <translation>toistaa aac-tiedostoja, tukee raakoja aac-tiedostoja sekä mp4-säilöä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="35"/>
        <source>Adplug player (ADLIB OPL2/OPL3 emulator)</source>
        <translation>Adplug soitin (ADLIB OPL2/OPL3 emulaattori)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="41"/>
        <source>plays alac files from MP4 and M4A files</source>
        <translation>toistaa alac-tiedostoja MP4- ja M4A-tiedostoista</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="45"/>
        <source>plays psf, psf2, spu, ssf, dsf, qsf file formats</source>
        <translation>toistaa psf, psf2, spu, ssf, dsf, qsf tiedostomuotoja</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="49"/>
        <source>Loads album artwork from embedded tags, local directories, or internet services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="63"/>
        <source>plays dts-encoded files using libdca from VLC project</source>
        <translation>toistaa dts-koodattuja tiedostoja käyttämällä VLC-projektin libdca-kirjastoa</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="67"/>
        <source>User interface implemented using Sailfish Silica QML module</source>
        <translation>Sailfishin Silica QML moduulilla toteutettu käyttöliittymä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="71"/>
        <source>High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/</source>
        <translation>Korkealaatuinen näytetaajuusmuunnin, käyttää libsampleratea, http://www.mega-nerd.com/SRC/</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="75"/>
        <source>module player based on DUMB library</source>
        <translation>DUMB-kirjastoon perustuva moduulisoitin</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="83"/>
        <source>APE player based on code from libavc and rockbox</source>
        <translation>APE-soitin perustuu libavc- ja rockbox-koodiin</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="87"/>
        <source>decodes audio formats using FFMPEG libavcodec</source>
        <translation>dekoodaa ääniformaatit käyttämällä FFMPEG libavcodec:ia</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="93"/>
        <source>FLAC decoder using libFLAC</source>
        <translation>FLAC-dekooderi, joka käyttää libFLAC:ia</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="99"/>
        <source>chiptune/game music player based on GME library</source>
        <translation>chiptune/pelimusiikkisoitin perustuen GME-kirjastoon</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="107"/>
        <source>Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol</source>
        <translation>Lähettää soitettujen kappaleiden tiedot last.fm-tilillesi tai muulle AudioScrobbler-protokollaa käyttävälle palvelulle</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="119"/>
        <source>Importing and exporting M3U and PLS formats
Recognizes .pls, .m3u and .m3u8 file types

NOTE: only utf8 file names are currently supported</source>
        <translation>M3U- ja PLS-muotojen tuonti ja vienti
Tunnistaa .pls-, .m3u- ja .m3u8-tiedostotyypit

HUOMIO: Vain utf8-tiedostonimiä tuetaan tällä hetkellä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="123"/>
        <source>MMS streaming plugin based on libmms</source>
        <translation>Libms-pohjainen MMS-suoratoistolaajennus</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="127"/>
        <source>Mono to stereo converter DSP</source>
        <translation>DSP Mono-stereomuunnin</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="131"/>
        <source>MPEG v1/2 layer1/2/3 decoder

Using libmpg123 backend.
</source>
        <translation>MPEG v1 / 2 layer1 / 2/3 -dekooderi

Perustuu libmpg123:seen
</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="137"/>
        <source>Communicate with other applications using D-Bus.</source>
        <translation>Kommunikoi muiden sovellusten kanssa D-Busin avulla.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="141"/>
        <source>Musepack decoder using libmppdec</source>
        <translation>Musepack-dekooderi, joka käyttää libmppdec:ia</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="145"/>
        <source>This plugin takes the audio data, and discards it,
so nothing will play.
This is useful for testing.</source>
        <translation>Tämä liitännäinen ottaa vastaan ja hylää äänidatan,
jolloin mikään ei soi.
Tästä on hyötyä testatessa.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="149"/>
        <source>Opus player based on libogg, libopus and libopusfile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="153"/>
        <source>At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.
If you experience problems - please try switching to ALSA or OSS output.
If that doesn&apos;t help - please uninstall PulseAudio from your system, and try ALSA or OSS again.
Thanks for understanding</source>
        <translation>Tällä hetkellä (kirjoituksen aikana) PulseAudio näyttää olevan erittäin epävakaa monissa (tai useimmissa) GNU / Linux-jakeluissa.
Jos kohtaat ongelmia yritä vaihtaa ALSA- tai OSS-ulostuloon.
Jos tämä ei auttanut poista PulseAudio ja yritä ALSA:aa tai OSS:aa uudelleen.
Kiitos</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="159"/>
        <source>SC68 player (Atari ST SNDH YM2149)</source>
        <translation>SC68 soitin (Atari ST SNDH YM2149)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="166"/>
        <source>decodes shn files</source>
        <translation>shn-tiedostojen dekoodaaja</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="173"/>
        <source>SID player based on libsidplay2</source>
        <translation>SID-soitin, joka perustuu libsidplay2:een</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="183"/>
        <source>wav/aiff player using libsndfile</source>
        <translation>wav/aiff-soitin, joka käyttää libsndfileä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="188"/>
        <source>Tempo/Pitch/Rate changer using SoundTouch Library (http://www.surina.net/soundtouch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="192"/>
        <source>equalizer plugin using SuperEQ library</source>
        <translation>taajuuskorjain lisäosa joka käyttää SuperEQ-kirjastoa</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="196"/>
        <source>tta decoder based on TTA Hardware Players Library Version 1.2</source>
        <translation>tta-dekooderi, joka perustuu TTA Hardware Players Library versioon 1.2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="200"/>
        <source>http and ftp streaming module using libcurl, with ICY protocol support</source>
        <translation>http ja ftp-suoratoistomoduuli perustuen libcurliin, ICY-protokollan tuella</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="205"/>
        <source>Standard IO plugin
Used for reading normal local files
It is statically linked, so you can&apos;t delete it.</source>
        <translation>Standardi IO-liitännäinen
Käytetään paikallisten tiedostojen lukemiseen
Se on staattisesti linkitetty, joten et voi poistaa sitä.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="209"/>
        <source>play files directly from zip files</source>
        <translation>toistaa tiedostoja suoraan zip-tiedostoista</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="213"/>
        <source>Ogg Vorbis decoder using standard xiph.org libraries</source>
        <translation>Ogg Vorbis-dekooderi joka käyttää vakio xiph.org-kirjastoja</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="217"/>
        <source>AY8910/12 chip emulator and vtx file player</source>
        <translation>AY8910 / 12 piiriemulaattori ja vtx-tiedostosoitin</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="222"/>
        <source>WavPack (.wv, .iso.wv) player</source>
        <translation>WavPack (.wv, .iso.wv) soitin</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="226"/>
        <source>MIDI player based on WildMidi library

Requires freepats package to be installed
See http://freepats.zenvoid.org/
Make sure to set correct freepats.cfg path in plugin settings.</source>
        <translation>MIDI-soitin, joka perustuu WildMidi-kirjastoon

Vaatii freepats-paketin asentamisen
Katso http://freepats.zenvoid.org/
Varmista, että olet asettanut oikean freepats.cfg-polun lisäosan asetuksista.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="231"/>
        <source>plays WMA files</source>
        <translation>toistaa WMA tiedostoja</translation>
    </message>
</context>
<context>
    <name>PluginSettings</name>
    <message>
        <location filename="../qml/dialogs/PluginSettings.qml" line="15"/>
        <source>%1 settings</source>
        <translation>%1 asetukset</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="108"/>
        <source>Enable scrobbler</source>
        <translation>Ota scrobbler käyttöön</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="109"/>
        <source>Disable nowplaying</source>
        <translation>Poista nowplaying käytöstä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="110"/>
        <source>Username</source>
        <translation>Käyttäjänimi</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="111"/>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="112"/>
        <source>Scrobble URL</source>
        <translation>Scrobble URL</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="113"/>
        <source>Prefer Album Artist over Artist field</source>
        <translation>Suosi &quot;albumin artisti&quot;-metatietoa &quot;artisti&quot;-metatiedon sijaan</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="114"/>
        <source>Send MusicBrainz ID</source>
        <translation>Lähetä MusicBrainz ID</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="115"/>
        <source>Submit tracks shorter than 30 seconds (not recommended)</source>
        <translation>Lähetä kappaleet jotka ovat lyhyempiä kuin 30 sekuntia (ei suositella)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="100"/>
        <source>Max song length (in minutes)</source>
        <translation>Kappaleen maksimipituus (minuuteissa)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="101"/>
        <source>Fadeout length (seconds)</source>
        <translation>Häivytyksen pituus (sekuneissa)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="102"/>
        <source>Play loops nr. of times (if available)</source>
        <translation>Toista n. kertaa (jos saatavilla)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="155"/>
        <source>Preferred buffer size</source>
        <translation>Haluttu bufferin koko</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="167"/>
        <source>Relative seek table path</source>
        <translation>Suhteellinen seek table-polku</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="168"/>
        <source>Absolute seek table path</source>
        <translation>Absoluuttinen seek table-polku</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="169"/>
        <source>Swap audio bytes (toggle if all you hear is static)</source>
        <translation>Vaihda äänitavut (valitse jos kuulet vain lumisadetta)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="174"/>
        <source>Enable HVSC Songlength DB</source>
        <translation>Ota HVSC Songlength DB käyttöön</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="161"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="176"/>
        <source>Samplerate</source>
        <translation>Näytetaajuus</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="178"/>
        <source>Mono synth</source>
        <translation>Monosynteesi</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="179"/>
        <source>Default song length (sec)</source>
        <translation>Kappaleen oletuspituus (sek)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="201"/>
        <source>Enable logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="227"/>
        <source>Timidity++ bank configuration file</source>
        <translation>Timidity++ pankin määritystiedosto</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="88"/>
        <source>Use all extensions supported by ffmpeg</source>
        <translation>Käytä kaikkia ffmpegin tukemia laajennuksia</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="89"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="184"/>
        <source>File Extensions (separate with &apos;;&apos;)</source>
        <translation>Tiedostotunnisteet (erottele &apos;;&apos; merkillä)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="132"/>
        <source>Backend</source>
        <translation>Taustaohjelmisto</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="36"/>
        <source>Prefer Ken emu over Satoh (surround won&apos;t work)</source>
        <translation>Suosi Ken emu:a Satoh:in sijaan (surround ei toimi)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="37"/>
        <source>Enable surround</source>
        <translation>Ota surround käyttöön</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="50"/>
        <source>Cache update period (in hours, 0=never)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="51"/>
        <source>Fetch from embedded tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="52"/>
        <source>Fetch from local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="53"/>
        <source>Fetch from Last.fm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="54"/>
        <source>Fetch from MusicBrainz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="55"/>
        <source>Fetch from Albumart.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="56"/>
        <source>Fetch from World of Spectrum (AY files only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="57"/>
        <source>When no artwork is found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="58"/>
        <source>Custom image path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="59"/>
        <source>Scale artwork towards longer side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="76"/>
        <source>Resampling quality</source>
        <translation>Näytteen laatu</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="77"/>
        <source>8-bit output</source>
        <translation>8-bittinen ulostulo</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="78"/>
        <source>Internal DUMB volume</source>
        <translation>Sisäinen DUMB äänenvoimakkuus</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="79"/>
        <source>Volume ramping</source>
        <translation>Äänenvoimakkuuden nousu</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="94"/>
        <source>Ignore bad header errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="95"/>
        <source>Ignore unparsable stream errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="103"/>
        <source>ColecoVision BIOS (for SGC file format)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="133"/>
        <source>Force 16 bit output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="154"/>
        <source>PulseAudio server (leave empty for default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="160"/>
        <source>Default song length (in minutes)</source>
        <translation>Kappaleen oletuspituus (minuuteissa)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="162"/>
        <source>Skip when shorter than (sec)</source>
        <translation>Ylitä jos lyhyempi kuin (sek)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="175"/>
        <source>Full path to Songlengths.md5/.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="177"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="218"/>
        <source>Bits per sample</source>
        <translation>Bittejä per näyte</translation>
    </message>
</context>
<context>
    <name>Plugins</name>
    <message>
        <location filename="../qml/dialogs/Plugins.qml" line="11"/>
        <source>Plugins</source>
        <translation>Lisäosat</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="29"/>
        <source>Playback options</source>
        <translation>Toiston asetukset</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="33"/>
        <source>Looping</source>
        <translation>Toisto</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="42"/>
        <source>Don&apos;t loop</source>
        <translation>Älä toista</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="48"/>
        <source>Loop single song</source>
        <translation>Toista yksittäistä kappaletta</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="54"/>
        <source>Loop all</source>
        <translation>Toista kaikki</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="61"/>
        <source>Playback order</source>
        <translation>Toistojärjestys</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="71"/>
        <source>Linear</source>
        <translation>Lineaarinen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="77"/>
        <source>Shuffle tracks</source>
        <translation>Sekoita kappaleita</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="83"/>
        <source>Shuffle albums</source>
        <translation>Sekoita albumeja</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="89"/>
        <source>Random</source>
        <translation>Satunnainen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="97"/>
        <source>Resume previous session on startup</source>
        <translation>Palauta edellinen istunto käynnistyksessä</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="112"/>
        <source>Pause playback when headset is unplugged</source>
        <translation>Pysäytä toisto kun kuulokkeet irrotetaan</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="119"/>
        <source>Resume playback when headset is plugged</source>
        <translation>Jatka toistoa kun kuulokkeet kiinnitetään</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="126"/>
        <source>View options</source>
        <translation>Valikon asetukset</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="139"/>
        <source>Playlist grouping</source>
        <translation>Soittolistojen ryhmitys</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="149"/>
        <source>None</source>
        <translation>Ei mitään</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="155"/>
        <source>Artist/Date/Album</source>
        <translation>Artisti/päivämäärä/albumi</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="161"/>
        <source>Artist</source>
        <translation>Artisti</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="167"/>
        <source>Custom</source>
        <translation>Muokattu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="182"/>
        <source>Format</source>
        <translation>Muoto</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="197"/>
        <source>Please use &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; spec as a reference on format fields for grouping</source>
        <translation>Käytä &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; mallia referenssinä ryhmityksen muokkaamiseen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="130"/>
        <source>Use simple one-line playlist items</source>
        <translation>Käytä yksirivisiä soittolistojen nimikkeitä</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="211"/>
        <source>Display album art for artist/album groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="218"/>
        <source>Display album art on cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="251"/>
        <source>Setup Last.fm</source>
        <translation>Last.fm asetukset</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="261"/>
        <source>Plugins</source>
        <translation>Lisäosat</translation>
    </message>
</context>
<context>
    <name>TrackProperties</name>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="28"/>
        <source>Track Properties</source>
        <translation>Kappaleen ominaisuudet</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="45"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="49"/>
        <source>No metadata</source>
        <translation>Ei metadataa</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="67"/>
        <source>Properties</source>
        <translation>Ominaisuudet</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="71"/>
        <source>No properties</source>
        <translation>Ei ominaisuuksia</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="8"/>
        <source>Artist</source>
        <translation>Artisti</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="9"/>
        <source>Track Title</source>
        <translation>Kappaleen otsikko</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="10"/>
        <source>Album</source>
        <translation>Albumi</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="11"/>
        <source>Date</source>
        <translation>Päivämäärä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="12"/>
        <source>Track Number</source>
        <translation>Kappalenumero</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="13"/>
        <source>Total Tracks</source>
        <translation>Kappalemäärä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="14"/>
        <source>Genre</source>
        <translation>Tyylilaji</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="15"/>
        <source>Composer</source>
        <translation>Säveltäjä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="16"/>
        <source>Disc Number</source>
        <translation>Levyn numero</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="17"/>
        <source>Total Disks</source>
        <translation>Levyjä</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="18"/>
        <source>Comment</source>
        <translation>Kommentit</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="22"/>
        <source>Location</source>
        <translation>Sijainti</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="23"/>
        <source>Subtrack Index</source>
        <translation>Alakappale indeksi</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="24"/>
        <source>Duration</source>
        <translation>Kesto</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="25"/>
        <source>Tag Type(s)</source>
        <translation>Tagien tyypi(t)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="26"/>
        <source>Embedded Cuesheet</source>
        <translation>Sisällytetty .cue-tiedosto</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="27"/>
        <source>Codec</source>
        <translation>Kodekki</translation>
    </message>
</context>
</TS>
