import QtQuick 2.0
import org.nemomobile.policy 1.0
import Sailfish.Media 1.0
import org.nemomobile.systemsettings 1.0    // Hope I won't regret importing this

import deadbeef 1.0

// This is how global hotkeys are handled in Sailfish
// FIXME: Can we use patched hotkeys plugin for more flexible hotkeys configuration?

Item {
    AboutSettings {
        id: aboutSettings

        property int versionMajor: softwareVersionId.split(".")[0]
        property int versionMinor: softwareVersionId.split(".")[1]

        property bool mprisControlSupported: (versionMajor == 4 && versionMajor >= 2) || versionMajor > 4
    }

    Permissions {
        enabled: aboutSettings.mprisControlSupported === false
        applicationClass: "player"

        Resource {
            id: headsetKeysResource
            type: Resource.HeadsetButtons
            optional: true
        }
    }

    MultiClickMediaKey {
        key: Qt.Key_ToggleCallHangup
        enabled: headsetKeysResource.acquired
        onSingleClick: DdbApi.playPause()
        onDoubleClick: DdbApi.next()
        onTripleClick: DdbApi.prev()
    }

    MediaKey {
        key: Qt.Key_MediaTogglePlayPause
        enabled: headsetKeysResource.acquired
        onReleased: DdbApi.playPause()
    }

    MediaKey {
        key: Qt.Key_MediaPlay
        enabled: headsetKeysResource.acquired
        onReleased: DdbApi.play()
    }

    MediaKey {
        key: Qt.Key_MediaPause
        enabled: headsetKeysResource.acquired
        onReleased: DdbApi.pause()
    }

    MediaKey {
        key: Qt.Key_MediaStop
        enabled: headsetKeysResource.acquired
        onReleased: DdbApi.stop()
    }

    MediaKey {
        key: Qt.Key_MediaPrevious
        enabled: headsetKeysResource.acquired
        onReleased: DdbApi.prev()
    }

    MediaKey {
        key: Qt.Key_MediaNext
        enabled: headsetKeysResource.acquired
        onReleased: DdbApi.next()
    }

    // FIXME: Implement rewind
}
