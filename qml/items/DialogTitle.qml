import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    property int fontSize: Theme.fontSizeLarge

    anchors {
        left: parent.left
        leftMargin: Theme.horizontalPageMargin
        right: parent.right
        rightMargin: Theme.horizontalPageMargin
    }

    font.pixelSize: fontSize
    wrapMode: Text.Wrap
    color: Theme.highlightColor
    opacity: text.length > 0 ? 1.0 : 0.0
    Behavior on opacity {
        FadeAnimation {}
    }
}
