#ifndef __FS_H
#define __FS_H

#include <QObject>

class CFs : public QObject
{
    Q_OBJECT
public:
    explicit CFs(QObject *parent = nullptr);

    Q_INVOKABLE bool pathExists(QString path);
    Q_INVOKABLE bool isDir(QString path);

    Q_INVOKABLE QString getAbsoluteOrCurrent(QString path);
};

#endif // __FS_H
