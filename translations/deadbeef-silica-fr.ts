<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/dialogs/About.qml" line="27"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="42"/>
        <source>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 This program is licensed under the terms of GPLv3 license
                                 
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues                                  tracker to file bug reports and feature requests.
                                 
                                 Credits:
                                 
                                 %3</source>
        <translation>Extension DeadBeef-Silica GUI %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 Ce programme est licencié sous les termes de la licence GPLv3
                                 
                                 Veuillez utiliser https://bitbucket.org/kravich/deadbeef-silica/issues                                  pour signaler des bugs et demander de nouvelles fonctionnalités.
                                 
                                 Crédits :
                                 
                                 %3</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="53"/>
        <source>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Russian translation</source>
        <translation>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Traduction en russe</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="59"/>
        <source>Allan Nordhøy
                                                    Norwegian Bokmål translation</source>
        <translation>Allan Nordhøy
                                                    Traduction en norvégien</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="67"/>
        <source>Rui Kon
                                                    Elizabeth Sherrock
                                                    Chinese translation</source>
        <translation>Rui Kon
                                                    Elizabeth Sherrock
                                                    Traduction en mandarin</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="70"/>
        <source>Szabó G
                                                    Hungarian translation</source>
        <translation>Szabó G
                                                    Traduction en hongrois</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="56"/>
        <source>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Spanish translation</source>
        <translation>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Traduction en espagnol</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="61"/>
        <source>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    French translation</source>
        <translation>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    Traduction en français</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="72"/>
        <source>O S
                                                    J. Lavoie
                                                    German translation</source>
        <translation>O S
                                                    J. Lavoie
                                                    Traduction en allemand</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="75"/>
        <source>Åke Engelbrektson
                                                     Swedish translation</source>
        <translation>Åke Engelbrektson
                                                     Traduction en suédois</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="77"/>
        <source>Reima Vesterinen
                                                     Finnish translation</source>
        <translation>Reima Vesterinen
                                                     Traduction en finnois</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="79"/>
        <source>Milo Ivir
                                                     Croatian translation</source>
        <translation>Milo Ivir
                                                     Traduction en croate</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="120"/>
        <source>DeadBeef License</source>
        <translation>Licence DeadBeef</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="131"/>
        <source>DeadBeef is licensed under the terms of zlib license. For more information please refer to %1</source>
        <translation>DeadBeef est distribué sous les termes de la licence zlib. Pour plus d&apos;informations, veuillez vous référer à %1</translation>
    </message>
</context>
<context>
    <name>AddLocation</name>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="21"/>
        <source>Add location</source>
        <translation>Ajouter un emplacement</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="41"/>
        <source>Location (URI or file path)</source>
        <translation>Emplacement (URL ou le chemin du fichier)</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>kHz</source>
        <translation>kHz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="20"/>
        <source>Zero all</source>
        <translation>Tout remettre à zéro</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="47"/>
        <source>Equalizer</source>
        <translation>Égaliseur</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="59"/>
        <source>Enable</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="109"/>
        <source>Preamp</source>
        <translation>Préampli</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>+%1 dB</source>
        <translation>+%1 dB</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="55"/>
        <source>Hide system files</source>
        <translation>Masquer les fichiers système</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="56"/>
        <source>Show system files</source>
        <translation>Afficher les fichiers système</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="65"/>
        <source>Select current dir</source>
        <translation>Choisir ce dossier</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="66"/>
        <source>Select</source>
        <translation>Choisir</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="84"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="108"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="13"/>
        <source>untitled</source>
        <comment>default name of the file to be saved</comment>
        <translation>sans titre</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="106"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="130"/>
        <source>Select file location</source>
        <translation>Sélectionnez l&apos;emplacement du fichier</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="145"/>
        <source>New file name</source>
        <translation>Nouveau nom de fichier</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="163"/>
        <source>File exists. Overwrite?</source>
        <translation>Le fichier existe. Le remplacer ?</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="178"/>
        <source>There is already a directory with such name</source>
        <translation>Un dossier de ce nom existe déjà</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="194"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
</context>
<context>
    <name>FileSelectionField</name>
    <message>
        <location filename="../qml/items/FileSelectionField.qml" line="39"/>
        <source>Select %1</source>
        <comment>title of dialog that selects path to file in plugin settings</comment>
        <translation>Sélectionner %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <source>Supported formats</source>
        <translation>Formats supportés</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>Supported playlist formats</source>
        <translation>Formats de listes de lecture pris en charge</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Open file(s)/folder(s)</source>
        <comment>item of pull-up menu</comment>
        <translation>Ouvrir fichier(s)/dossier(s)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Open file(s)/folder(s)</source>
        <comment>title of dialog page</comment>
        <translation>Ouverture de fichier(s)/dossier(s)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="105"/>
        <source>Load playlist</source>
        <comment>item of pull-up menu</comment>
        <translation>Charger la liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="109"/>
        <source>Load playlist</source>
        <comment>title of dialog page</comment>
        <translation>Chargement liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>More…</source>
        <translation>Plus…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>Empty playlist</source>
        <translation>Liste de lecture vide</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="137"/>
        <source>Pull down to add files</source>
        <translation>Tirez vers le bas pour ajouter des fichiers</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="203"/>
        <source>Add To Playback Queue</source>
        <translation>Ajouter à la file d&apos;attente de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="208"/>
        <source>Remove From Playback Queue</source>
        <translation>Retirer de la file d&apos;attente de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="216"/>
        <source>Track Properties</source>
        <translation>Propriétés de la piste</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="222"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="229"/>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>Equalizer</source>
        <translation>Égaliseur</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="272"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="277"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
</context>
<context>
    <name>PlaylistActions</name>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="30"/>
        <source>Supported formats</source>
        <translation>Formats pris en charge</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="31"/>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="70"/>
        <source>Playlist actions</source>
        <translation>Opération de liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="77"/>
        <source>Add file(s)/folder(s)</source>
        <comment>playlist actions menu item</comment>
        <translation>Ajouter fichier(s)/dossier(s)</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="79"/>
        <source>Save playlist</source>
        <comment>playlist actions menu item</comment>
        <translation>Enregistrer la liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="90"/>
        <source>Add file(s)/folder(s)</source>
        <comment>file add dialog title</comment>
        <translation>Ajouter fichier(s)/dossier(s)</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="108"/>
        <source>Save playlist</source>
        <comment>save playlist dialog title</comment>
        <translation>Enregistrement de la liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="78"/>
        <source>Add location</source>
        <translation>Ajouter un emplacement</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="80"/>
        <source>Sort playlist</source>
        <translation>Trier la liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="81"/>
        <source>Clear playlist</source>
        <translation>Effacer la liste de lecture</translation>
    </message>
</context>
<context>
    <name>PlaylistNameEdit</name>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Add playlist</source>
        <translation>Ajout d&apos;une liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Rename playlist</source>
        <translation>Renommage de la liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="42"/>
        <source>Playlist name</source>
        <translation>Nom de la liste de lecture</translation>
    </message>
</context>
<context>
    <name>PlaylistSorting</name>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="16"/>
        <source>Sort by</source>
        <translation>Trier par</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="29"/>
        <source>Reverse order</source>
        <translation>Inverse l&apos;ordre</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="45"/>
        <source>Artist/Album/Track number</source>
        <translation>Artiste/Album/numéro de piste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="46"/>
        <source>Artist/Album</source>
        <translation>Artiste/Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="47"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="48"/>
        <source>Track number</source>
        <translation>Numéro de piste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="49"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="50"/>
        <source>Artist</source>
        <translation>Artiste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="51"/>
        <source>Album artist</source>
        <translation>Artiste de l&apos;album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="52"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="53"/>
        <source>Filename</source>
        <translation>Nom du fichier</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="40"/>
        <source>New playlist</source>
        <translation>Nouvelle liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="19"/>
        <source>Add new playlist</source>
        <translation>Ajouter une nouvelle liste de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="67"/>
        <source>Playlists</source>
        <translation>Listes de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="94"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="114"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="121"/>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
</context>
<context>
    <name>PluginCopyright</name>
    <message>
        <location filename="../qml/dialogs/PluginCopyright.qml" line="24"/>
        <source>Copyright</source>
        <translation>Droit d&apos;auteur</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="29"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="53"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="59"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="82"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="90"/>
        <source>Copyright</source>
        <translation>Droit d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="31"/>
        <source>plays aac files, supports raw aac files, as well as mp4 container</source>
        <translation>lit les fichiers AAC, support des fichiers raw AAC et des conteneurs MP4</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="35"/>
        <source>Adplug player (ADLIB OPL2/OPL3 emulator)</source>
        <translation>Lecteur adplug (émulateur ADLIB - opl2 / opl3)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="41"/>
        <source>plays alac files from MP4 and M4A files</source>
        <translation>lit les fichiers ALAC à partir des conteneurs MP4 et M4A</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="45"/>
        <source>plays psf, psf2, spu, ssf, dsf, qsf file formats</source>
        <translation>Lit les formats PSF, PSF2, SPU, SSF, DSF, QSF</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="49"/>
        <source>Loads album artwork from embedded tags, local directories, or internet services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="63"/>
        <source>plays dts-encoded files using libdca from VLC project</source>
        <translation>lit les fichiers encodés par DTS via la bibliothèque libdca du projet VLC</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="67"/>
        <source>User interface implemented using Sailfish Silica QML module</source>
        <translation>Interface utilisateur mise en œuvre en utilisant le module Silica QML de SailfishOS</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="71"/>
        <source>High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/</source>
        <translation>Convertisseur d’échantillonnage de haute qualité utilisant libsamplerate, http://www.mega-nerd.com/SRC/</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="75"/>
        <source>module player based on DUMB library</source>
        <translation>module du lecteur basé sur la bibliothèque DUMB</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="83"/>
        <source>APE player based on code from libavc and rockbox</source>
        <translation>Lecteur APE basé sur code provenant de libavc et rockbox</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="87"/>
        <source>decodes audio formats using FFMPEG libavcodec</source>
        <translation>Décode les formats audio en utilisant libavcodec de FFMPEG</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="93"/>
        <source>FLAC decoder using libFLAC</source>
        <translation>Décodeur FLAC utilisant libFLAC</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="99"/>
        <source>chiptune/game music player based on GME library</source>
        <translation>Lecteur de musique chiptune/game basé sur la bibliothèque GME library</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="107"/>
        <source>Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol</source>
        <translation>Envoie les données des morceaux lus vers votre compte last.fm ou tout autre service utilisant le protocole AudioScrobbler</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="119"/>
        <source>Importing and exporting M3U and PLS formats
Recognizes .pls, .m3u and .m3u8 file types

NOTE: only utf8 file names are currently supported</source>
        <translation>Importation et exportation de formats M3U et PLS
Reconnaît les types de fichiers .pls, .m3u et .m3u8

REMARQUE : seuls les noms de fichiers utf8 sont actuellement pris en charge</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="123"/>
        <source>MMS streaming plugin based on libmms</source>
        <translation>Plugin de streaming MMS basé sur libmms</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="127"/>
        <source>Mono to stereo converter DSP</source>
        <translation>Conversion mono vers stéréo</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="131"/>
        <source>MPEG v1/2 layer1/2/3 decoder

Using libmpg123 backend.
</source>
        <translation>Décodeur MPEG v1/2 couche1/2/2/3

Utilise le backend delibmpg123.
</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="137"/>
        <source>Communicate with other applications using D-Bus.</source>
        <translation>Communiquer avec d&apos;autres applications en utilisant D-Bus.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="141"/>
        <source>Musepack decoder using libmppdec</source>
        <translation>Décodeur Musepack utilisant libmppdec</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="145"/>
        <source>This plugin takes the audio data, and discards it,
so nothing will play.
This is useful for testing.</source>
        <translation>Cette extension récupère et élimine les données audio,
aucune lecture donc.
Utile en phase de test.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="149"/>
        <source>Opus player based on libogg, libopus and libopusfile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="153"/>
        <source>At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.
If you experience problems - please try switching to ALSA or OSS output.
If that doesn&apos;t help - please uninstall PulseAudio from your system, and try ALSA or OSS again.
Thanks for understanding</source>
        <translation>PulseAudio semble actuellement instable dans de nombreux environnements GNU/Linux.
En cas de difficulté, essayez d&apos;utiliser une sortie ALSA ou OSS.
Si cela n&apos;aide pas, désinstallez PulseAudio avant de réessayer ALSA ou OSS.
Merci de votre compréhension</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="159"/>
        <source>SC68 player (Atari ST SNDH YM2149)</source>
        <translation>Lecteur SC68 (Atari ST SNDH YM2149)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="166"/>
        <source>decodes shn files</source>
        <translation>décode les fichiers shn</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="173"/>
        <source>SID player based on libsidplay2</source>
        <translation>Lecteur SID basé sur libsidplay2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="183"/>
        <source>wav/aiff player using libsndfile</source>
        <translation>Lecteur WAV / AIFF en utilisant libsndfile</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="188"/>
        <source>Tempo/Pitch/Rate changer using SoundTouch Library (http://www.surina.net/soundtouch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="192"/>
        <source>equalizer plugin using SuperEQ library</source>
        <translation>Greffon égaliseur utilisant la bibliothèque SuperEQ</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="196"/>
        <source>tta decoder based on TTA Hardware Players Library Version 1.2</source>
        <translation>décodeur tta qui s&apos;appuie sur la bibliothèque TTA Hardware Players version 1.2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="200"/>
        <source>http and ftp streaming module using libcurl, with ICY protocol support</source>
        <translation>module de streaming http et ftp utilisant libcurl, avec support du protocole ICY</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="205"/>
        <source>Standard IO plugin
Used for reading normal local files
It is statically linked, so you can&apos;t delete it.</source>
        <translation>Greffon standard d&apos;entrée-sortie, 
utilisé en lecture normale des fichiers locaux.
Ne peut être supprimé.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="209"/>
        <source>play files directly from zip files</source>
        <translation>Lit directement des fichiers contenus dans une archive ZIP</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="213"/>
        <source>Ogg Vorbis decoder using standard xiph.org libraries</source>
        <translation>Décodeur Ogg Vorbis utilisant la bibliothèque standard de xiph.org</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="217"/>
        <source>AY8910/12 chip emulator and vtx file player</source>
        <translation>émulateur de circuit AY8910/12 et joueur de fichier vtx</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="222"/>
        <source>WavPack (.wv, .iso.wv) player</source>
        <translation>Lecteur WavPack (.wv, .iso.wv)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="226"/>
        <source>MIDI player based on WildMidi library

Requires freepats package to be installed
See http://freepats.zenvoid.org/
Make sure to set correct freepats.cfg path in plugin settings.</source>
        <translation>Lecteur MIDI basé sur la bibliothèque WildMidi

Nécessite l’installation du paquet freepats
Voir http://freepats.zenvoid.org/
Assurez- vous de configurer correctement le chemin de freepats.cfg dans les paramètres de l’extension.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="231"/>
        <source>plays WMA files</source>
        <translation>Lit les fichiers WMA</translation>
    </message>
</context>
<context>
    <name>PluginSettings</name>
    <message>
        <location filename="../qml/dialogs/PluginSettings.qml" line="15"/>
        <source>%1 settings</source>
        <translation>Paramètres %1</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="108"/>
        <source>Enable scrobbler</source>
        <translation>Activer scrobbler</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="109"/>
        <source>Disable nowplaying</source>
        <translation>Désactiver nowplaying</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="110"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="111"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="112"/>
        <source>Scrobble URL</source>
        <translation>URL Scrobble</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="113"/>
        <source>Prefer Album Artist over Artist field</source>
        <translation>Préférer le champ &quot;Artiste de l&apos;album&quot; au champ &quot;Artiste&quot;</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="114"/>
        <source>Send MusicBrainz ID</source>
        <translation>Envoyer l&apos;identifiant MusicBrainz</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="115"/>
        <source>Submit tracks shorter than 30 seconds (not recommended)</source>
        <translation>Soumettre les pistes de moins de 30 secondes (non recommandé)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="100"/>
        <source>Max song length (in minutes)</source>
        <translation>Durée maximale des chansons (en minutes)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="101"/>
        <source>Fadeout length (seconds)</source>
        <translation>Durée du fondu de fermeture (secondes)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="102"/>
        <source>Play loops nr. of times (if available)</source>
        <translation>Jouer en boucle N fois (si disponible)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="155"/>
        <source>Preferred buffer size</source>
        <translation>Taille de tampon préférée</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="167"/>
        <source>Relative seek table path</source>
        <translation>Chemin de recherche relative</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="168"/>
        <source>Absolute seek table path</source>
        <translation>Chemin de recherche absolue</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="169"/>
        <source>Swap audio bytes (toggle if all you hear is static)</source>
        <translation>Permuter les octets audio (basculer si l&apos;écoute est gelée)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="174"/>
        <source>Enable HVSC Songlength DB</source>
        <translation>Activer HVSC Songlength DB</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="161"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="176"/>
        <source>Samplerate</source>
        <translation>Taux d&apos;échantillonnage</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="178"/>
        <source>Mono synth</source>
        <translation>Synthétiseur mono</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="179"/>
        <source>Default song length (sec)</source>
        <translation>Longueur par défaut des chansons (sec)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="201"/>
        <source>Enable logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="227"/>
        <source>Timidity++ bank configuration file</source>
        <translation>Fichier de configuration Timidity++</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="88"/>
        <source>Use all extensions supported by ffmpeg</source>
        <translation>Utiliser toutes les extensions supportées par ffmpeg</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="89"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="184"/>
        <source>File Extensions (separate with &apos;;&apos;)</source>
        <translation>Extensions de fichier (séparateur : &quot;;&quot; sans guillemets)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="132"/>
        <source>Backend</source>
        <translation>Backend</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="36"/>
        <source>Prefer Ken emu over Satoh (surround won&apos;t work)</source>
        <translation>Préférer Ken Emu à Satoh (le surround ne fonctionnera pas)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="37"/>
        <source>Enable surround</source>
        <translation>Activer le surround</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="50"/>
        <source>Cache update period (in hours, 0=never)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="51"/>
        <source>Fetch from embedded tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="52"/>
        <source>Fetch from local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="53"/>
        <source>Fetch from Last.fm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="54"/>
        <source>Fetch from MusicBrainz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="55"/>
        <source>Fetch from Albumart.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="56"/>
        <source>Fetch from World of Spectrum (AY files only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="57"/>
        <source>When no artwork is found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="58"/>
        <source>Custom image path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="59"/>
        <source>Scale artwork towards longer side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="76"/>
        <source>Resampling quality</source>
        <translation>Qualité de rééchantillonnage</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="77"/>
        <source>8-bit output</source>
        <translation>Sortie 8 bits</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="78"/>
        <source>Internal DUMB volume</source>
        <translation>Volume DUMB interne</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="79"/>
        <source>Volume ramping</source>
        <translation>Augmenter le volume</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="94"/>
        <source>Ignore bad header errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="95"/>
        <source>Ignore unparsable stream errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="103"/>
        <source>ColecoVision BIOS (for SGC file format)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="133"/>
        <source>Force 16 bit output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="154"/>
        <source>PulseAudio server (leave empty for default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="160"/>
        <source>Default song length (in minutes)</source>
        <translation>Durée par défaut (en minutes)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="162"/>
        <source>Skip when shorter than (sec)</source>
        <translation>Omettre si plus court que... (en secondes)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="175"/>
        <source>Full path to Songlengths.md5/.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="177"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="218"/>
        <source>Bits per sample</source>
        <translation>Bits par échantillon</translation>
    </message>
</context>
<context>
    <name>Plugins</name>
    <message>
        <location filename="../qml/dialogs/Plugins.qml" line="11"/>
        <source>Plugins</source>
        <translation>Extensions</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="29"/>
        <source>Playback options</source>
        <translation>Options de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="33"/>
        <source>Looping</source>
        <translation>Boucle</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="42"/>
        <source>Don&apos;t loop</source>
        <translation>Ne pas boucler</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="48"/>
        <source>Loop single song</source>
        <translation>Boucler sur une seule chanson</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="54"/>
        <source>Loop all</source>
        <translation>Tout boucler</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="61"/>
        <source>Playback order</source>
        <translation>Ordre de lecture</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="71"/>
        <source>Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="77"/>
        <source>Shuffle tracks</source>
        <translation>Lecture aléatoire des pistes</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="83"/>
        <source>Shuffle albums</source>
        <translation>Lecture aléatoire des albums</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="89"/>
        <source>Random</source>
        <translation>Aléatoire</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="97"/>
        <source>Resume previous session on startup</source>
        <translation>Reprendre la session précédente au démarrage</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="112"/>
        <source>Pause playback when headset is unplugged</source>
        <translation>Mettre la lecture en pause lorsque le casque est débranché</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="119"/>
        <source>Resume playback when headset is plugged</source>
        <translation>Reprendre la lecture lorsque le casque est branché</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="126"/>
        <source>View options</source>
        <translation>Voir les options</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="139"/>
        <source>Playlist grouping</source>
        <translation>Groupement de playlists</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="149"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="155"/>
        <source>Artist/Date/Album</source>
        <translation>Artiste/Date/Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="161"/>
        <source>Artist</source>
        <translation>Artiste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="167"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="182"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="197"/>
        <source>Please use &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; spec as a reference on format fields for grouping</source>
        <translation>Veuillez utiliser &lt;a href=&quot;%1&quot;&gt;cette&lt;/a&gt; spécification comme référence sur le format des champs pour les groupements</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="130"/>
        <source>Use simple one-line playlist items</source>
        <translation>Utiliser des éléments de playlist en une seule ligne</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="211"/>
        <source>Display album art for artist/album groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="218"/>
        <source>Display album art on cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="251"/>
        <source>Setup Last.fm</source>
        <translation>Configuration de Last.fm</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="261"/>
        <source>Plugins</source>
        <translation>Extensions</translation>
    </message>
</context>
<context>
    <name>TrackProperties</name>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="28"/>
        <source>Track Properties</source>
        <translation>Propriétés de la Piste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="45"/>
        <source>Metadata</source>
        <translation>Métadonnées</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="49"/>
        <source>No metadata</source>
        <translation>Aucune métadonnée</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="67"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="71"/>
        <source>No properties</source>
        <translation>Aucune propriété</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="8"/>
        <source>Artist</source>
        <translation>Artiste</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="9"/>
        <source>Track Title</source>
        <translation>Titre de la piste</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="10"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="11"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="12"/>
        <source>Track Number</source>
        <translation>N° de piste</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="13"/>
        <source>Total Tracks</source>
        <translation>Nombre total de pistes</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="14"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="15"/>
        <source>Composer</source>
        <translation>Compositeur</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="16"/>
        <source>Disc Number</source>
        <translation>Disque n°</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="17"/>
        <source>Total Disks</source>
        <translation>Nombre total de disques</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="18"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="22"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="23"/>
        <source>Subtrack Index</source>
        <translation>Sous-index</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="24"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="25"/>
        <source>Tag Type(s)</source>
        <translation>Type(s) d&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="26"/>
        <source>Embedded Cuesheet</source>
        <translation>Fiche signalétique incorporée</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="27"/>
        <source>Codec</source>
        <translation>CODEC</translation>
    </message>
</context>
</TS>
