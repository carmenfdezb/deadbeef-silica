<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/dialogs/About.qml" line="27"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="42"/>
        <source>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 This program is licensed under the terms of GPLv3 license
                                 
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues                                  tracker to file bug reports and feature requests.
                                 
                                 Credits:
                                 
                                 %3</source>
        <translation>DeadBeef-Silica GUI plugin %1
                                 版权所有© %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 该项目使用 GPLv3 许可协议
                                 
                                 请使用 https://bitbucket.org/kravich/deadbeef-silica/issues                                  反馈 bug 及请求新功能
                                 
                                 荣誉:
                                 
                                 %3</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="53"/>
        <source>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Russian translation</source>
        <translation>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    俄语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="59"/>
        <source>Allan Nordhøy
                                                    Norwegian Bokmål translation</source>
        <translation>Allan Nordhøy
                                                    书面挪威语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="67"/>
        <source>Rui Kon
                                                    Elizabeth Sherrock
                                                    Chinese translation</source>
        <translation>Rui Kon
                                                    Elizabeth Sherrock
                                                    汉语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="70"/>
        <source>Szabó G
                                                    Hungarian translation</source>
        <translation>Szabó G
                                                    匈牙利语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="56"/>
        <source>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Spanish translation</source>
        <translation>Carmen Fdez. B.
                                                    J. Lavoie
                                                    西班牙语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="61"/>
        <source>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    French translation</source>
        <translation>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    法语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="72"/>
        <source>O S
                                                    J. Lavoie
                                                    German translation</source>
        <translation>O S
                                                    J. Lavoie
                                                    德语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="75"/>
        <source>Åke Engelbrektson
                                                     Swedish translation</source>
        <translation>Åke Engelbrektson
瑞典语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="77"/>
        <source>Reima Vesterinen
                                                     Finnish translation</source>
        <translation>Reima Vesterinen
                                                     芬兰语翻译</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="79"/>
        <source>Milo Ivir
                                                     Croatian translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="120"/>
        <source>DeadBeef License</source>
        <translation>DeadBeef 许可协议</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="131"/>
        <source>DeadBeef is licensed under the terms of zlib license. For more information please refer to %1</source>
        <translation>DeadBeef 采用 zlib 许可协议。有关更多信息，请参阅 %1 。</translation>
    </message>
</context>
<context>
    <name>AddLocation</name>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="21"/>
        <source>Add location</source>
        <translation>添加位置</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="41"/>
        <source>Location (URI or file path)</source>
        <translation>位置（URI 或文件路径）</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>kHz</source>
        <translation>kHz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="20"/>
        <source>Zero all</source>
        <translation>全部归零</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="47"/>
        <source>Equalizer</source>
        <translation>均衡器</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="59"/>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="109"/>
        <source>Preamp</source>
        <translation>预放大器</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>+%1 dB</source>
        <translation>+%1 dB</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="55"/>
        <source>Hide system files</source>
        <translation>隐藏系统文件</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="56"/>
        <source>Show system files</source>
        <translation>显示系统文件</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="65"/>
        <source>Select current dir</source>
        <translation>选择当前目录</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="66"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="84"/>
        <source>Filter</source>
        <translation>筛选器</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="108"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="13"/>
        <source>untitled</source>
        <comment>default name of the file to be saved</comment>
        <translation>无题</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="106"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="130"/>
        <source>Select file location</source>
        <translation>选择文件位置</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="145"/>
        <source>New file name</source>
        <translation>新文件名</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="163"/>
        <source>File exists. Overwrite?</source>
        <translation>文件存在。是否覆盖？</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="178"/>
        <source>There is already a directory with such name</source>
        <translation>已经有一个同名目录</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="194"/>
        <source>Format</source>
        <translation>格式</translation>
    </message>
</context>
<context>
    <name>FileSelectionField</name>
    <message>
        <location filename="../qml/items/FileSelectionField.qml" line="39"/>
        <source>Select %1</source>
        <comment>title of dialog that selects path to file in plugin settings</comment>
        <translation>选择 %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <source>Supported formats</source>
        <translation>支持的格式</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <source>All files</source>
        <translation>所有文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>Supported playlist formats</source>
        <translation>支持的播放列表格式</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Open file(s)/folder(s)</source>
        <comment>item of pull-up menu</comment>
        <translation>打开文件/文件夹</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Open file(s)/folder(s)</source>
        <comment>title of dialog page</comment>
        <translation>打开文件/文件夹</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="105"/>
        <source>Load playlist</source>
        <comment>item of pull-up menu</comment>
        <translation>加载播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="109"/>
        <source>Load playlist</source>
        <comment>title of dialog page</comment>
        <translation>加载播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>More…</source>
        <translation>更多…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>Empty playlist</source>
        <translation>播放列表为空</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="137"/>
        <source>Pull down to add files</source>
        <translation>下拉以添加文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="203"/>
        <source>Add To Playback Queue</source>
        <translation>添加到播放队列</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="208"/>
        <source>Remove From Playback Queue</source>
        <translation>从播放队列中删除</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="216"/>
        <source>Track Properties</source>
        <translation>音乐属性</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="222"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="229"/>
        <source>Deleting</source>
        <translation>正在删除</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>Equalizer</source>
        <translation>均衡器</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="272"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="277"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>PlaylistActions</name>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="30"/>
        <source>Supported formats</source>
        <translation>支持的格式</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="31"/>
        <source>All files</source>
        <translation>所有文件</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="70"/>
        <source>Playlist actions</source>
        <translation>播放列表操作</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="77"/>
        <source>Add file(s)/folder(s)</source>
        <comment>playlist actions menu item</comment>
        <translation>添加文件/文件夹</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="79"/>
        <source>Save playlist</source>
        <comment>playlist actions menu item</comment>
        <translation>保存播放列表</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="90"/>
        <source>Add file(s)/folder(s)</source>
        <comment>file add dialog title</comment>
        <translation>添加文件/文件夹</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="108"/>
        <source>Save playlist</source>
        <comment>save playlist dialog title</comment>
        <translation>保存播放列表</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="78"/>
        <source>Add location</source>
        <translation>添加位置</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="80"/>
        <source>Sort playlist</source>
        <translation>排序播放列表</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="81"/>
        <source>Clear playlist</source>
        <translation>清空播放器列表</translation>
    </message>
</context>
<context>
    <name>PlaylistNameEdit</name>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Add playlist</source>
        <translation>添加播放列表</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Rename playlist</source>
        <translation>重命名播放列表</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="42"/>
        <source>Playlist name</source>
        <translation>播放列表名称</translation>
    </message>
</context>
<context>
    <name>PlaylistSorting</name>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="16"/>
        <source>Sort by</source>
        <translation>排序方式</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="29"/>
        <source>Reverse order</source>
        <translation>反转顺序</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="45"/>
        <source>Artist/Album/Track number</source>
        <translation>艺术家/专辑/音乐编号</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="46"/>
        <source>Artist/Album</source>
        <translation>艺术家/专辑</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="47"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="48"/>
        <source>Track number</source>
        <translation>音乐编号</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="49"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="50"/>
        <source>Artist</source>
        <translation>艺人</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="51"/>
        <source>Album artist</source>
        <translation>专辑艺人</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="52"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="53"/>
        <source>Filename</source>
        <translation>文件名</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="40"/>
        <source>New playlist</source>
        <translation>新播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="19"/>
        <source>Add new playlist</source>
        <translation>添加新播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="67"/>
        <source>Playlists</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="94"/>
        <source>Rename</source>
        <translation>重 命名</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="114"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="121"/>
        <source>Deleting</source>
        <translation>正在删除...</translation>
    </message>
</context>
<context>
    <name>PluginCopyright</name>
    <message>
        <location filename="../qml/dialogs/PluginCopyright.qml" line="24"/>
        <source>Copyright</source>
        <translation>版权所有</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="29"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="53"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="59"/>
        <source>Website</source>
        <translation>网站</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="82"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="90"/>
        <source>Copyright</source>
        <translation>版权</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="31"/>
        <source>plays aac files, supports raw aac files, as well as mp4 container</source>
        <translation>播放 aac 文件,支持原始 aac 文件,以及 mp4 容器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="35"/>
        <source>Adplug player (ADLIB OPL2/OPL3 emulator)</source>
        <translation>Adplug 播放器(ADLIB OPL2/OPL3 仿真器)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="41"/>
        <source>plays alac files from MP4 and M4A files</source>
        <translation>播放MP4和M4A文件中的alac文件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="45"/>
        <source>plays psf, psf2, spu, ssf, dsf, qsf file formats</source>
        <translation>播放 psf, psf2, spu, sf, dsf, qsf 文件格式</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="49"/>
        <source>Loads album artwork from embedded tags, local directories, or internet services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="63"/>
        <source>plays dts-encoded files using libdca from VLC project</source>
        <translation>使用 VLC 项目中的 libdca 播放 dts 编码文件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="67"/>
        <source>User interface implemented using Sailfish Silica QML module</source>
        <translation>使用旗鱼系统 Silica QML模块实现的用户界面</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="71"/>
        <source>High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/</source>
        <translation>使用自由采样率的高质量采样率转换器,http://www.mega-nerd.com/SRC/</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="75"/>
        <source>module player based on DUMB library</source>
        <translation>基于 DUMB 库的模块播放器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="83"/>
        <source>APE player based on code from libavc and rockbox</source>
        <translation>APE播放器基于libavc和rockbox的代码</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="87"/>
        <source>decodes audio formats using FFMPEG libavcodec</source>
        <translation>使用 FFMPEG libavcodec 解码音频格式</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="93"/>
        <source>FLAC decoder using libFLAC</source>
        <translation>使用 libFLAC 的 FLAC 解码器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="99"/>
        <source>chiptune/game music player based on GME library</source>
        <translation>基于GME库的芯片/游戏音乐播放器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="107"/>
        <source>Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol</source>
        <translation>将播放的音乐信息发送到 last.fm 帐户或其他使用 audioScrobbler 协议的服务</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="119"/>
        <source>Importing and exporting M3U and PLS formats
Recognizes .pls, .m3u and .m3u8 file types

NOTE: only utf8 file names are currently supported</source>
        <translation>导入和导出M3U和PLS格式
识别.pls，.m3u和.m3u8文件类型

注意：目前仅支持utf8文件名</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="123"/>
        <source>MMS streaming plugin based on libmms</source>
        <translation>基于 libmms 的MMS流媒体插件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="127"/>
        <source>Mono to stereo converter DSP</source>
        <translation>单声道到立体声转换器 DSP</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="131"/>
        <source>MPEG v1/2 layer1/2/3 decoder

Using libmpg123 backend.
</source>
        <translation>MPEG v1/2 层1/2/3解码器

使用 libmpg123 后端。
</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="137"/>
        <source>Communicate with other applications using D-Bus.</source>
        <translation>使用 D-Bus 与其他应用程序通信。</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="141"/>
        <source>Musepack decoder using libmppdec</source>
        <translation>使用 libmppdec 的 Musepack 解码器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="145"/>
        <source>This plugin takes the audio data, and discards it,
so nothing will play.
This is useful for testing.</source>
        <translation>这个插件获取音频数据并丢弃它，
所以不会播放什么。
这对测试很有用。</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="149"/>
        <source>Opus player based on libogg, libopus and libopusfile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="153"/>
        <source>At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.
If you experience problems - please try switching to ALSA or OSS output.
If that doesn&apos;t help - please uninstall PulseAudio from your system, and try ALSA or OSS again.
Thanks for understanding</source>
        <translation>在撰写本文时，PulseAudio在许多（或大多数）GNU / Linux发行版中似乎非常不稳定。
如果您遇到问题 - 请尝试切换到ALSA或OSS输出。
如果这没有帮助 - 请从您的系统中卸载PulseAudio，然后再次尝试ALSA或OSS。
感谢您的理解</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="159"/>
        <source>SC68 player (Atari ST SNDH YM2149)</source>
        <translation>SC68播放器（Atari ST SNDH YM2149）</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="166"/>
        <source>decodes shn files</source>
        <translation>解码 shn 文件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="173"/>
        <source>SID player based on libsidplay2</source>
        <translation>基于 libsidplay2 的 SID 播放器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="183"/>
        <source>wav/aiff player using libsndfile</source>
        <translation>wav/aiff  播放器使用 libsndfile</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="188"/>
        <source>Tempo/Pitch/Rate changer using SoundTouch Library (http://www.surina.net/soundtouch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="192"/>
        <source>equalizer plugin using SuperEQ library</source>
        <translation>使用 SuperEQ 库的均衡器插件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="196"/>
        <source>tta decoder based on TTA Hardware Players Library Version 1.2</source>
        <translation>基于 TTA 硬件播放器库版本 1.2 的塔解码器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="200"/>
        <source>http and ftp streaming module using libcurl, with ICY protocol support</source>
        <translation>使用 libcurl 的 http 和 ftp 流模块,支持 ICY 协议</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="205"/>
        <source>Standard IO plugin
Used for reading normal local files
It is statically linked, so you can&apos;t delete it.</source>
        <translation>标准 IO 插件
用于读取正常本地文件
它是静态链接的,因此您无法将其删除。</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="209"/>
        <source>play files directly from zip files</source>
        <translation>直接从 zip 文件播放文件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="213"/>
        <source>Ogg Vorbis decoder using standard xiph.org libraries</source>
        <translation>使用标准 xiph.org 库的 Ogg Vorbis 解码器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="217"/>
        <source>AY8910/12 chip emulator and vtx file player</source>
        <translation>AY8910/12 芯片仿真器和 vtx 文件播放器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="222"/>
        <source>WavPack (.wv, .iso.wv) player</source>
        <translation>WavPack（.wv，.iso.wv）播放器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="226"/>
        <source>MIDI player based on WildMidi library

Requires freepats package to be installed
See http://freepats.zenvoid.org/
Make sure to set correct freepats.cfg path in plugin settings.</source>
        <translation>基于WildMidi库的MIDI播放器

需要安装freepats包
见http://freepats.zenvoid.org/
确保在插件设置中设置正确的freepats.cfg路径。</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="231"/>
        <source>plays WMA files</source>
        <translation>播放 WMA 文件</translation>
    </message>
</context>
<context>
    <name>PluginSettings</name>
    <message>
        <location filename="../qml/dialogs/PluginSettings.qml" line="15"/>
        <source>%1 settings</source>
        <translation>%1 设置</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="108"/>
        <source>Enable scrobbler</source>
        <translation>开启 scrobbler</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="109"/>
        <source>Disable nowplaying</source>
        <translation>禁用正在播放</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="110"/>
        <source>Username</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="111"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="112"/>
        <source>Scrobble URL</source>
        <translation>Scrobble URL</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="113"/>
        <source>Prefer Album Artist over Artist field</source>
        <translation>在艺人字段中倾向于使用“专辑艺人”</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="114"/>
        <source>Send MusicBrainz ID</source>
        <translation>发送 MusicBrainz ID</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="115"/>
        <source>Submit tracks shorter than 30 seconds (not recommended)</source>
        <translation>加入少于30秒的歌曲（不推荐）</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="100"/>
        <source>Max song length (in minutes)</source>
        <translation>最大歌曲长度(以分钟计)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="101"/>
        <source>Fadeout length (seconds)</source>
        <translation>淡出长度（秒）</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="102"/>
        <source>Play loops nr. of times (if available)</source>
        <translation>播放循环 nr.时间(如果可用)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="155"/>
        <source>Preferred buffer size</source>
        <translation>首选缓冲区大小</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="167"/>
        <source>Relative seek table path</source>
        <translation>相对查找表路径</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="168"/>
        <source>Absolute seek table path</source>
        <translation>绝对查找表路径</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="169"/>
        <source>Swap audio bytes (toggle if all you hear is static)</source>
        <translation>交换音频字节（如果你听到的都是静态的，则切换）</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="174"/>
        <source>Enable HVSC Songlength DB</source>
        <translation>启用 HVSC 歌曲长度数据库</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="161"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="176"/>
        <source>Samplerate</source>
        <translation>采样率</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="178"/>
        <source>Mono synth</source>
        <translation>单声道合成器</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="179"/>
        <source>Default song length (sec)</source>
        <translation>默认音乐长度(秒)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="201"/>
        <source>Enable logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="227"/>
        <source>Timidity++ bank configuration file</source>
        <translation>Timidity++ bank 配置文件</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="88"/>
        <source>Use all extensions supported by ffmpeg</source>
        <translation>使用 ffmpeg 支持的所有扩展</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="89"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="184"/>
        <source>File Extensions (separate with &apos;;&apos;)</source>
        <translation>文件扩展名（以&apos;;&apos;分隔）</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="132"/>
        <source>Backend</source>
        <translation>后端</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="36"/>
        <source>Prefer Ken emu over Satoh (surround won&apos;t work)</source>
        <translation>首选 Satoh 的 Ken emu  (环绕不工作)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="37"/>
        <source>Enable surround</source>
        <translation>启用环绕</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="50"/>
        <source>Cache update period (in hours, 0=never)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="51"/>
        <source>Fetch from embedded tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="52"/>
        <source>Fetch from local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="53"/>
        <source>Fetch from Last.fm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="54"/>
        <source>Fetch from MusicBrainz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="55"/>
        <source>Fetch from Albumart.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="56"/>
        <source>Fetch from World of Spectrum (AY files only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="57"/>
        <source>When no artwork is found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="58"/>
        <source>Custom image path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="59"/>
        <source>Scale artwork towards longer side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="76"/>
        <source>Resampling quality</source>
        <translation>重采样质量</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="77"/>
        <source>8-bit output</source>
        <translation>8 位输出</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="78"/>
        <source>Internal DUMB volume</source>
        <translation>内部 DUMB 卷</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="79"/>
        <source>Volume ramping</source>
        <translation>音量渐变</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="94"/>
        <source>Ignore bad header errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="95"/>
        <source>Ignore unparsable stream errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="103"/>
        <source>ColecoVision BIOS (for SGC file format)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="133"/>
        <source>Force 16 bit output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="154"/>
        <source>PulseAudio server (leave empty for default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="160"/>
        <source>Default song length (in minutes)</source>
        <translation>默认音乐长度(以分钟计)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="162"/>
        <source>Skip when shorter than (sec)</source>
        <translation>跳过短于以下时长（秒）的音乐</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="175"/>
        <source>Full path to Songlengths.md5/.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="177"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="218"/>
        <source>Bits per sample</source>
        <translation>每个样本的比特</translation>
    </message>
</context>
<context>
    <name>Plugins</name>
    <message>
        <location filename="../qml/dialogs/Plugins.qml" line="11"/>
        <source>Plugins</source>
        <translation>插件</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="29"/>
        <source>Playback options</source>
        <translation>播放选项</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="33"/>
        <source>Looping</source>
        <translation>循环</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="42"/>
        <source>Don&apos;t loop</source>
        <translation>不循环</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="48"/>
        <source>Loop single song</source>
        <translation>单曲循环</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="54"/>
        <source>Loop all</source>
        <translation>全部循环</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="61"/>
        <source>Playback order</source>
        <translation>播放顺序</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="71"/>
        <source>Linear</source>
        <translation>顺序</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="77"/>
        <source>Shuffle tracks</source>
        <translation>随机播放歌曲</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="83"/>
        <source>Shuffle albums</source>
        <translation>随机播放专辑</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="89"/>
        <source>Random</source>
        <translation>随机</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="97"/>
        <source>Resume previous session on startup</source>
        <translation>启动时恢复之前的会话</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="112"/>
        <source>Pause playback when headset is unplugged</source>
        <translation>拔下耳机时暂停播放</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="119"/>
        <source>Resume playback when headset is plugged</source>
        <translation>耳机插入时恢复播放</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="126"/>
        <source>View options</source>
        <translation>查看选项</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="139"/>
        <source>Playlist grouping</source>
        <translation>播放列表分组</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="149"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="155"/>
        <source>Artist/Date/Album</source>
        <translation>艺人/日期/专辑</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="161"/>
        <source>Artist</source>
        <translation>艺人</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="167"/>
        <source>Custom</source>
        <translation>自 定义</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="182"/>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="197"/>
        <source>Please use &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; spec as a reference on format fields for grouping</source>
        <translation>请使用 &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; 规范作为格式字段的参考，以便分组</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="130"/>
        <source>Use simple one-line playlist items</source>
        <translation>使用简单的单行播放列表项目</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="211"/>
        <source>Display album art for artist/album groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="218"/>
        <source>Display album art on cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="251"/>
        <source>Setup Last.fm</source>
        <translation>设置 Last.fm</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="261"/>
        <source>Plugins</source>
        <translation>插件</translation>
    </message>
</context>
<context>
    <name>TrackProperties</name>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="28"/>
        <source>Track Properties</source>
        <translation>音乐属性</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="45"/>
        <source>Metadata</source>
        <translation>元数据</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="49"/>
        <source>No metadata</source>
        <translation>无元数据</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="67"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="71"/>
        <source>No properties</source>
        <translation>无属性</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="8"/>
        <source>Artist</source>
        <translation>艺人</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="9"/>
        <source>Track Title</source>
        <translation>音乐标题</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="10"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="11"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="12"/>
        <source>Track Number</source>
        <translation>音乐编号</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="13"/>
        <source>Total Tracks</source>
        <translation>全部音乐</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="14"/>
        <source>Genre</source>
        <translation>曲风</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="15"/>
        <source>Composer</source>
        <translation>作曲家</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="16"/>
        <source>Disc Number</source>
        <translation>唱片号码</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="17"/>
        <source>Total Disks</source>
        <translation>全部唱片</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="18"/>
        <source>Comment</source>
        <translation>评论</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="22"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="23"/>
        <source>Subtrack Index</source>
        <translation>子轨道索引</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="24"/>
        <source>Duration</source>
        <translation>时长</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="25"/>
        <source>Tag Type(s)</source>
        <translation>标签类型</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="26"/>
        <source>Embedded Cuesheet</source>
        <translation>嵌入提示页</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="27"/>
        <source>Codec</source>
        <translation>编解码器</translation>
    </message>
</context>
</TS>
