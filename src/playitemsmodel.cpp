#include "playitemsmodel.h"

#include "logger.h"

#include <QTimer>
#include <QUrl>

CPlayitemsModel::CPlayitemsModel(DB_functions_t *api):
    CAbstractQmlListModel(NULL),
    m_api(api),
    m_currentItemsCount(0),
    m_currentTrackIdx(-1),
    m_groupingMode(GroupingNone),
    m_artworkPlugin(NULL)
{
    LOG_DBG_FUNC("%p", this);

    qRegisterMetaType<char*>("char*");

    m_defaultArtworkPath = QString("file://") + QUrl::toPercentEncoding(QString(m_api->get_system_dir(DDB_SYS_DIR_PIXMAP)) + "/" + "noartwork.png");

    LOG_DBG("Static default artwork: %s", m_defaultArtworkPath.toUtf8().data());

    updateGroupingMode();
    updateCustomGroupingFormatString();
    onPlaylistChanged();
}

CPlayitemsModel::~CPlayitemsModel()
{
    LOG_DBG_FUNC("%p", this);
}

int CPlayitemsModel::nowPlayingIdx()
{
    return m_currentTrackIdx;
}

QString CPlayitemsModel::nowPlayingTitle()
{
    QString title;

    DB_playItem_t *it = m_api->streamer_get_playing_track();

    if (it)
    {
        title = produceTFormattedStringForPlayitem(it, "%title%");
        m_api->pl_item_unref(it);
    }

    return title;
}

QString CPlayitemsModel::nowPlayingArtistAlbum()
{
    QString artistAlbum;

    DB_playItem_t *it = m_api->streamer_get_playing_track();

    if (it)
    {
        artistAlbum = produceTFormattedStringForPlayitem(it, "$if(%artist%,%artist%,Unknown Artist)[ - %album%]");
        m_api->pl_item_unref(it);
    }

    return artistAlbum;
}

CPlayitemsModel::EGroupingMode CPlayitemsModel::groupingMode()
{
    return m_groupingMode;
}

void CPlayitemsModel::setGroupingMode(CPlayitemsModel::EGroupingMode mode)
{
    LOG_DBG_FUNC("mode=%d", mode);

    if (mode == m_groupingMode)
        return;

    m_groupingMode = mode;
    emit groupingModeChanged();

    UpdateCovers();

    emitGroupingKeyRoleChanged();
    // WA: Emit dataChanged() for GroupingKeyRole second time some time after to properly re-layout
    // the first item of the list which is not properly laid out after the first one
    QTimer::singleShot(16, this, &CPlayitemsModel::emitGroupingKeyRoleChanged);

    m_api->conf_set_int("silica.playlist.grouping_mode", (int)m_groupingMode);
    m_api->sendmessage(DB_EV_CONFIGCHANGED, 0, 0, 0);
}

QString CPlayitemsModel::customGroupingFormatString()
{
    return m_customGroupingFormatString;
}

void CPlayitemsModel::setCustomGroupingFormatString(QString formatString)
{
    LOG_DBG_FUNC("formatString=%s", formatString.toUtf8().data());

    if (formatString == m_customGroupingFormatString)
        return;

    m_customGroupingFormatString = formatString;
    emit customGroupingFormatStringChanged();

    if (m_groupingMode == GroupingCustom)
    {
        emitGroupingKeyRoleChanged();
        // WA
        QTimer::singleShot(16, this, &CPlayitemsModel::emitGroupingKeyRoleChanged);
    }

    m_api->conf_set_str("silica.playlist.custom_grouping_tf", m_customGroupingFormatString.toUtf8().data());
    m_api->sendmessage(DB_EV_CONFIGCHANGED, 0, 0, 0);
}

void CPlayitemsModel::sortByTf(QString fmt, ESortOrder sortOrder)
{
    m_api->pl_lock();

    ddb_playlist_t *plt = m_api->plt_get_curr();
    m_api->plt_sort_v2(plt, PL_MAIN, -1, fmt.toUtf8().data(), sortOrder == SortAscending  ? DDB_SORT_ASCENDING :
                                                              sortOrder == SortDescending ? DDB_SORT_DESCENDING :
                                                                                            DDB_SORT_RANDOM);
    m_api->plt_save_config(plt);
    m_api->plt_unref(plt);

    m_api->sendmessage(DB_EV_PLAYLISTCHANGED, 0, DDB_PLAYLIST_CHANGE_CONTENT, 0);

    m_api->pl_unlock();
}

void CPlayitemsModel::setArtworkPlugin(DB_artwork_plugin_t *artworkPlugin)
{
    LOG_DBG_FUNC("Setting artwork plugin");

    m_artworkPlugin = artworkPlugin;

    const char *defaultCoverPath = m_artworkPlugin->get_default_cover();

    if (defaultCoverPath)
    {
        m_defaultArtworkPath = QString("file://") + QUrl::toPercentEncoding(defaultCoverPath, "/");
        LOG_DBG("Plugin-provided default artwork: %s", m_defaultArtworkPath.toUtf8().data());
    }

    onPlaylistChanged();
}

int CPlayitemsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_currentItemsCount;
}

QVariant CPlayitemsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() < 0 ||
        index.row() >= m_currentItemsCount)
        return QVariant();

    LOG_DBG("Querying data of %d item for role %d", index.row(), role);

    if (role == FirstLineRole)
        return produceTFormattedStringForIndex(index.row(), "[(Q:%queue_indexes%) ][%tracknumber%. ]%title%");
    else if (role == SecondLineRole)
        return produceTFormattedStringForIndex(index.row(), "$if(%artist%,%artist%,Unknown Artist)[ - %album%]");
    else if (role == DurationRole)
        return produceTFormattedStringForIndex(index.row(), "%length%");
    else if (role == IsNowPlayingRole)
        return index.row() == m_currentTrackIdx;
    else if (role == IsInPlayqueueRole)
        return isInPlayqueue(index.row());
    else if (role == GroupingKeyRole)
    {
        QString groupingKey;

        if (m_groupingMode != GroupingNone)
        {
            QString title = produceTFormattedStringForIndex(index.row(), getGroupingFormatString());
            QString cover;

            if (m_groupingMode == GroupingArtistYearAlbum)
            {
                cover = m_defaultArtworkPath;

                if (m_groupArtworkPaths.contains(title))
                {
                    cover = m_groupArtworkPaths[title];
                }
            }

            groupingKey = QString("{\"title\": \"%1\", \"cover\": \"%2\"}").arg(title, cover);
        }

        LOG_DBG("groupingKey='%s'", groupingKey.toUtf8().data());

        return groupingKey;
    }
    else if (role == CoverUriRole)
        return m_artworkPaths[index.row()];

    return QVariant();
}

QString CPlayitemsModel::produceTFormattedStringForIndex(int itemIdx, const QString &format) const
{
    m_api->pl_lock();

    ddb_playItem_t *it = m_api->pl_get_for_idx(itemIdx);
    if (!it)
    {
        LOG_WARN("Can't get item for idx %d", itemIdx);
        m_api->pl_unlock();
        return "";
    }

    QString str = produceTFormattedStringForPlayitem(it, format);

    m_api->pl_item_unref(it);
    m_api->pl_unlock();

    return str;
}

QString CPlayitemsModel::produceTFormattedStringForPlayitem(DB_playItem_t *it, const QString &format) const
{
    ddb_tf_context_t tfCtx =
    {
        sizeof(ddb_tf_context_t),
        DDB_TF_CONTEXT_NO_DYNAMIC,
        it,
        NULL,
        0,
        0,
        PL_MAIN,
        0
    };

    char *tfBytecode = m_api->tf_compile(format.toUtf8().data());

    char output[256];
    int res = m_api->tf_eval(&tfCtx, tfBytecode, output, 256);

    m_api->tf_free(tfBytecode);

    if (res < 0)
    {
        LOG_ERR("Failed to produce titleformatted string, format: %s", format.toUtf8().data());
        return "";
    }

    return output;
}

bool CPlayitemsModel::isInPlayqueue(int itemIdx) const
{
    bool isInPlayqueue = false;

    DB_playItem_t *it = m_api->pl_get_for_idx(itemIdx);

    if (it)
    {
        isInPlayqueue = m_api->playqueue_test(it) >= 0;
        m_api->pl_item_unref(it);
    }
    else
    {
        LOG_ERR("Can't check if track with index %d is in playqueue: playitem query failed", itemIdx);
    }

    return isInPlayqueue;
}

QString CPlayitemsModel::getGroupingFormatString() const
{
    return m_groupingMode == GroupingArtistYearAlbum ? "%album artist% - ['['%year%']' ]%album%" :
           m_groupingMode == GroupingArtist          ? "%artist%" :
           m_groupingMode == GroupingCustom          ? m_customGroupingFormatString :
                                                       ""; // No grouping means using empty grouping format string
}

void CPlayitemsModel::updateGroupingMode()
{
    int groupingMode = m_api->conf_get_int("silica.playlist.grouping_mode", GroupingNone);

    LOG_DBG("Got groupingMode=%d from config", groupingMode);

    if (groupingMode == GroupingArtistYearAlbum ||
        groupingMode == GroupingArtist ||
        groupingMode == GroupingCustom)
        setGroupingMode((EGroupingMode)groupingMode);
    else
        setGroupingMode(GroupingNone);
}

void CPlayitemsModel::updateCustomGroupingFormatString()
{
    m_api->conf_lock();
    QString customGroupingTf = QString::fromUtf8(m_api->conf_get_str_fast("silica.playlist.custom_grouping_tf",
                                                                          "%album artist% - ['['%year%']' ]%album%"));
    m_api->conf_unlock();

    LOG_DBG("Got customGroupingTf=%s from config", customGroupingTf.toUtf8().data());

    setCustomGroupingFormatString(customGroupingTf);
}

void CPlayitemsModel::emitGroupingKeyRoleChanged()
{
    LOG_DBG("Emitting GroupingKeyRole change");

    QVector<int> changedRoles;
    changedRoles.append(GroupingKeyRole);

    emit dataChanged(index(0), index(m_currentItemsCount - 1), changedRoles);
}

QHash<int, QByteArray> CPlayitemsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[FirstLineRole] = "firstLine";
    roles[SecondLineRole] = "secondLine";
    roles[DurationRole] = "duration";
    roles[IsNowPlayingRole] = "isNowPlaying";
    roles[IsInPlayqueueRole] = "isInPlayqueue";
    roles[GroupingKeyRole] = "groupingKey";
    roles[CoverUriRole] = "coverUri";

    return roles;
}

void CPlayitemsModel::onPlaylistChanged()
{
    m_api->pl_lock();

    int newItemsCount = m_api->pl_getcount(PL_MAIN);

    int tracksChanged = std::min(m_currentItemsCount, newItemsCount);

    if (tracksChanged)
    {
        emit dataChanged(index(0), index(tracksChanged - 1));
    }

    if (newItemsCount > m_currentItemsCount)
    {
        beginInsertRows(QModelIndex(), tracksChanged, newItemsCount - 1);
        m_currentItemsCount = newItemsCount;
        endInsertRows();
    }
    else if (m_currentItemsCount > newItemsCount)
    {
        beginRemoveRows(QModelIndex(), tracksChanged, m_currentItemsCount - 1);
        m_currentItemsCount = newItemsCount;
        endRemoveRows();
    }

    UpdateCovers();

    // Playlist could be changed because network stream updated its track info,
    // so, emit track title/artist change signals
    emit nowPlayingTitleChanged();
    emit nowPlayingArtistAlbumChanged();

    m_api->pl_unlock();

    // WA
    QTimer::singleShot(16, this, &CPlayitemsModel::emitGroupingKeyRoleChanged);
}

void CPlayitemsModel::UpdateCovers()
{
    m_api->pl_lock();

    // Reset artwork paths
    m_artworkPaths.fill(m_defaultArtworkPath, m_currentItemsCount);
    m_groupArtworkPaths.clear();

    // Get artworks if plugin is available
    if (m_artworkPlugin)
    {
        for (int idx = 0; idx < m_currentItemsCount; idx++)
        {
            ddb_playItem_t *it = m_api->pl_get_for_idx(idx);

            const char *uri = m_api->pl_find_meta(it, ":URI");
            const char *artist = m_api->pl_find_meta(it, "artist");
            const char *album = m_api->pl_find_meta(it, "album");

            if (!album || album[0] == '\0') {
                album = m_api->pl_find_meta(it, "title");
            }

            LOG_DBG("Quering cover for item %d with uri=%s, artist=%s, album=%s", idx, uri, artist, album);

            SCoverReadyCbData *userData = new SCoverReadyCbData();

            userData->it = it;
            userData->playitemsModel = this;

            const char *coverPath = m_artworkPlugin->get_album_art(uri, artist, album, -1, CPlayitemsModel::OnCoverReadyCb, userData);

            if (coverPath)
            {
                LOG_DBG("Synchronous SetCover call");

                SetCover(idx, coverPath);

                m_api->pl_item_unref(it);
                delete userData;
            }
        }
    }

    m_api->pl_unlock();
}

void CPlayitemsModel::SetCover(int idx, QString coverPath)
{
    LOG_DBG_FUNC("idx=%d, coverPath=%s", idx, coverPath.toUtf8().data());

    coverPath = QUrl::toPercentEncoding(coverPath, "/");

    coverPath.prepend("file://");

    m_artworkPaths[idx] = coverPath;

    QVector<int> changedRoles;
    changedRoles.append(CoverUriRole);

    emit dataChanged(index(idx), index(idx), changedRoles);

    if (m_groupingMode == GroupingArtistYearAlbum)
    {
        // FIXME: Is it too slow?
        QString groupName = produceTFormattedStringForIndex(idx, getGroupingFormatString());

        if (!m_groupArtworkPaths.contains(groupName))
        {
            m_groupArtworkPaths[groupName] = coverPath;

            emitGroupingKeyRoleChanged();
        }
    }
}

void CPlayitemsModel::OnCoverReadyCb(const char *fname, const char *artist, const char *album, void *userData)
{
    LOG_DBG_FUNC("fname=%s, artist=%s, album=%s", fname, artist, album);

    SCoverReadyCbData *cbData = (SCoverReadyCbData*)userData;
    CPlayitemsModel *_this = cbData->playitemsModel;

    QMetaObject::invokeMethod(_this,
                              "OnCoverReadyCbSync",
                              Q_ARG(char*, fname ? strdup(fname) : NULL),
                              Q_ARG(char*, artist ? strdup(artist) : NULL),
                              Q_ARG(char*, album ? strdup(album) : NULL),
                              Q_ARG(void*, userData));
}

void CPlayitemsModel::OnCoverReadyCbSync(char *fname, char *artist, char *album, void *userData)
{
    LOG_DBG_FUNC("fname=%s, artist=%s, album=%s", fname, artist, album);

    SCoverReadyCbData *cbData = (SCoverReadyCbData*)userData;

    if (fname && artist && album)
    {
        m_api->pl_lock();

        int idx = m_api->pl_get_idx_of(cbData->it);

        if (idx >= 0 && idx < m_currentItemsCount)
        {
            const char *coverPath = m_artworkPlugin->get_album_art(fname, artist, album, -1, NULL, NULL);

            if (coverPath)
            {
                LOG_DBG("Deferred SetCover call");
                SetCover(idx, coverPath);
            }
        }

        m_api->pl_unlock();
    }

    free(fname);
    free(artist);
    free(album);

    m_api->pl_item_unref(cbData->it);

    delete cbData;
}

void CPlayitemsModel::onPlayqueueChanged()
{
    if (m_currentItemsCount == 0)
        return;

    QVector<int> changedRoles;
    changedRoles.append(FirstLineRole); // Because first line contains playqueue index

    emit dataChanged(index(0), index(m_currentItemsCount - 1), changedRoles);
}

void CPlayitemsModel::onTrackChanged(int newTrackIdx)
{
    if (newTrackIdx == m_currentTrackIdx)
        return;

    int oldCurrentTrackIdx = m_currentTrackIdx;
    m_currentTrackIdx = newTrackIdx;

    emit nowPlayingIdxChanged();
    emit nowPlayingTitleChanged();
    emit nowPlayingArtistAlbumChanged();

    QVector<int> changedRoles;
    changedRoles.append(IsNowPlayingRole);

    emit dataChanged(index(oldCurrentTrackIdx), index(oldCurrentTrackIdx), changedRoles);
    emit dataChanged(index(m_currentTrackIdx), index(m_currentTrackIdx), changedRoles);
}

void CPlayitemsModel::onConfigChanged()
{
    LOG_DBG_FUNC();
    updateGroupingMode();
    updateCustomGroupingFormatString();
}
