import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Plugins")
        }

        model: DdbApi.pluginsModel

        delegate: ListItem {
            id: listItem

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                text: name

                color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor
            }

            onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginInfo.qml"), {
                                          ptr: ptr,
                                          name: name,
                                          description: description,
                                          copyright: copyright,
                                          website: website,
                                          versionMajor: versionMajor,
                                          versionMinor: versionMinor,
                                          hasSettings: hasSettings
                                      })
        }

        VerticalScrollDecorator {}
    }
}
