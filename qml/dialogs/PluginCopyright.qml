import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property string name
    property string copyright

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: copyrightInfoColumn.height + Theme.paddingLarge

        Column {
            id: copyrightInfoColumn

            width: parent.width

            spacing: Theme.paddingLarge

            PageHeader {
                title: name
            }

            SectionHeader {
                text: qsTr("Copyright")
            }

            LinkedLabel {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                plainText: copyright
                color: Theme.highlightColor
                wrapMode: Text.Wrap
            }
        }

        VerticalScrollDecorator {}
    }

}
