import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    property Item mainPage

    // FIXME: Implement landscape layout and allow all orientations
    allowedOrientations: Orientation.Portrait

    SilicaListView {
        id: playlistsView

        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Add new playlist")

                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/PlaylistNameEdit.qml"));

                    dialog.playlistName = generateNewPlaylistName();

                    dialog.creationMode = true;
                    dialog.accepted.connect(function() {
                        var playlistIdx = DdbApi.createPlaylist(dialog.playlistName);
                        if (playlistIdx >= 0)
                            DdbApi.selectPlaylist(playlistIdx);
                    });

                    dialog.acceptDestinationAction = PageStackAction.Pop;
                    dialog.acceptDestination = mainPage;
                }

                function generateNewPlaylistName() {
                    var model = playlistsView.model;

                    var nameTemplate = qsTr("New playlist");
                    var maxInt = Math.pow(2, 53) - 1;

                    var proposedName = nameTemplate;

                    for (var attempt = 0;; attempt++) {
                        if (attempt > 0)
                            proposedName = nameTemplate + " " + attempt;

                        var found = false;

                        for (var i = 0; i < model.count; i++)
                            if (model.get(i).title === proposedName) {
                                found = true;
                                break;
                            }

                        if (!found || attempt === maxInt)
                            break;
                    }

                    return proposedName;
                }
            }
        }

        header: PageHeader {
            title: qsTr("Playlists")
        }

        model: DdbApi.playlistsModel

        delegate: ListItem {
            id: listItem

            Label {
                x: Theme.horizontalPageMargin
                anchors.verticalCenter: parent.verticalCenter
                text: title
                color: isActive || listItem.highlighted ? Theme.highlightColor :
                                                          Theme.primaryColor;
            }

            onClicked: {
                DdbApi.selectPlaylist(index);
                pageStack.popAttached(undefined, PageStackAction.Animated);
            }

            menu: contextMenuComponent

            Component {
                id: contextMenuComponent
                ContextMenu {
                    MenuItem {
                        text: qsTr("Rename")
                        onClicked: {
                            // FIXME: Why do we need to save these references?
                            var ddbApi = DdbApi;
                            var playlistIdx = index;

                            var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/PlaylistNameEdit.qml"));

                            dialog.playlistName = title;

                            dialog.accepted.connect(function() {
                                ddbApi.playlistsModel.setProperty(playlistIdx, "title", dialog.playlistName);
                            });

                            dialog.acceptDestinationAction = PageStackAction.Pop;
                            dialog.acceptDestination = mainPage;
                        }
                    }

                    MenuItem {
                        text: qsTr("Delete")
                        onClicked: remove()
                    }
                }
            }

            function remove() {
                remorseAction(qsTr("Deleting"), function() { DdbApi.deletePlaylist(index); })
            }
        }

        VerticalScrollDecorator {}
    }
}
