import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

CoverBackground {
    Loader {
        anchors.fill: parent

        sourceComponent: DdbApi.playbackState === DdbApi.PlaybackStopped ? stoppedItem :
                                                                           nowPlayingItem
    }

    Component {
        id: stoppedItem

        Item {
            anchors.fill: parent

            Label {
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge

                y: (parent.height - coverActionArea.height) / 2

                text: "DeadBeef"
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    Component {
        id: nowPlayingItem

        Item {
            anchors.fill: parent

            property real textImplicitHeight: tracknameLabel.implicitHeight + artistAlbumLabel.implicitHeight
            property real textAreaHeight: height - coverActionArea.height
            property bool textFits: textImplicitHeight <= textAreaHeight

            Image {
                visible: DdbApi.displayAlbumArtOnCover

                anchors.fill: parent

                source: DdbApi.playitemsModel.nowPlayingIdx >= 0 ? DdbApi.playitemsModel.get(DdbApi.playitemsModel.nowPlayingIdx).coverUri : ""

                fillMode: Image.PreserveAspectCrop
            }

            Rectangle {
                visible: DdbApi.displayAlbumArtOnCover

                anchors.fill: parent
                gradient: Gradient {
                    GradientStop { position: 0.0; color: Qt.rgba(0, 0, 0, Theme.opacityHigh) }
                    GradientStop { position: 0.5; color: Qt.rgba(0, 0, 0, Theme.opacityLow) }
                    GradientStop { position: 1.0; color: Qt.rgba(0, 0, 0, 0) }
                }
            }

            Label {
                id: tracknameLabel

                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge

                y: textFits ? (textAreaHeight - textImplicitHeight) / 2 : 0

                height: {
                    if (textFits || implicitHeight < textAreaHeight / 2)
                        return implicitHeight;

                    if (artistAlbumLabel.implicitHeight >= textAreaHeight / 2)
                        return textAreaHeight / 2;

                    return textAreaHeight - artistAlbumLabel.implicitHeight;
                }

                text: DdbApi.playitemsModel.nowPlayingtTitle
                color: Theme.primaryColor
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom

                font.pixelSize: Theme.fontSizeLarge
            }

            Label {
                id: artistAlbumLabel

                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
                anchors.top: tracknameLabel.bottom

                height: {
                    if (textFits || implicitHeight < textAreaHeight / 2)
                        return implicitHeight;

                    if (tracknameLabel.implicitHeight >= textAreaHeight / 2)
                        return textAreaHeight / 2;

                    return textAreaHeight - tracknameLabel.implicitHeight;
                }

                text: DdbApi.playitemsModel.nowPlayingArtistAlbum
                color: Theme.highlightColor
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignTop

                font.pixelSize: Theme.fontSizeLarge
            }
        }
    }

    CoverActionList {
        id: coverActions

        CoverAction {
            iconSource: "image://theme/icon-cover-" + (DdbApi.playbackState == DdbApi.PlaybackPlaying ? "pause" :
                                                                                                        "play")
            onTriggered: DdbApi.playPause()
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-next-song"
            onTriggered: DdbApi.next()
        }
    }
}
