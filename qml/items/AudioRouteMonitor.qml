import QtQuick 2.0
import org.nemomobile.dbus 2.0

Item {
    property string outputDevice: "unknown" // strings are ok for such low-frequency signals

    // Use custom signal instead of standard onOutputDeviceChanged to be able
    // to avoid it's emission during initCurrentRoute
    signal routeChanged

    DBusInterface {
        bus: DBus.SystemBus

        service: "org.nemomobile.Route.Manager"
        path: "/org/nemomobile/Route/Manager"
        iface: "org.nemomobile.Route.Manager"

        signalsEnabled: true

        signal audioRouteChanged(string name, int routeMask)

        onAudioRouteChanged: {
            //console.log("name=" + name + ", routeMask=" + routeMask);

            if (isOutputRouteMask(routeMask)) {
                var newOutputDevice = routeMaskToDevice(routeMask);

                if (outputDevice !== newOutputDevice) {
                    outputDevice = newOutputDevice;
                    routeChanged();
                }
            }
        }

        // route masks:
        //  5 - 00101 - phone speaker out
        //  9 - 01001 - headset out
        // 17 - 10001 - bluetooth out
        //  6 - 00110 - phone mic in
        // 10 - 01010 - headset mic in

        function isOutputRouteMask(routeMask) {
            return routeMask & 0x1;
        }

        function routeMaskToDevice(routeMask) {
            return routeMask & 0x04 ? "phone"   :
                   routeMask & 0x08 ? "headset"   :
                   routeMask & 0x10 ? "bluetooth" :
                                      "unknown";
        }

        function initCurrentRoute() {
            typedCall("ActiveRoutes",
                      [{"type": "s", "value": ""},
                       {"type": "u", "value": 0},
                       {"type": "s", "value": ""},
                       {"type": "u", "value": 0}],
                      function (outputName, outputRouteMask, inputName, inputRouteMask) {
                          outputDevice = routeMaskToDevice(outputRouteMask);
                          //console.log("Initialized outputDevice with " + outputDevice);
                      },
                      function () {
                          console.log("Failed to query current output device");
                      });
        }

        Component.onCompleted: initCurrentRoute()
    }
}
