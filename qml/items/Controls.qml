import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: root

    signal prevClicked
    signal playPauseClicked
    signal nextClicked
    signal seekPerformed(real position)
    signal playbackOrderClicked
    signal playbackModeClicked

    property real playbackPosition: 0.0
    property real playbackDuration: 0.0

    readonly property bool seekPossible: playbackDuration !== 0

    width: parent.width
    height: controlsColumn.height + 2 * Theme.paddingSmall

    state: "stopped"

    PanelBackground { anchors.fill: parent }

    Column {
        id: controlsColumn

        width: parent.width

        y: parent.height - height - Theme.paddingSmall

        Slider {
            id: seekbar

            width: parent.width
            handleVisible: false

            // When seek is not possible, set maximumValue to something larger than value
            // to avoid warning
            maximumValue: seekPossible ? playbackDuration : 1.0
            value: seekPossible ? playbackPosition : 0.0

            enabled: seekPossible

            onDownChanged: function() {
                if (down) {
                    // Break binding
                    value = value;
                }
                else {
                    // Restore binding
                    value = Qt.binding(function() { return playbackPosition; })
                }
            }

            onValueChanged: if (down) { root.seekPerformed(value) }

            valueText: ""

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }

            function msToMinutesAndSeconds(ms) {
                var minutes = Math.floor(ms / 1000 / 60);
                var seconds = Math.floor(ms / 1000 % 60);

                return minutes + ":" + pad(seconds, 2);
            }
        }

        Row {
            id: controlsRow

            anchors.horizontalCenter: parent.horizontalCenter

            spacing: Theme.paddingLarge

            // FIXME: Why Row is not automatically resized to fit large button?
            height: Math.max(prevButton.height,
                             playPauseButton.height,
                             nextButton.height) + Theme.paddingLarge

            IconButton {
                id: prevButton
                icon.source: "image://theme/icon-m-previous"
                onClicked: root.prevClicked()
            }

            IconButton {
                id: playPauseButton
                icon.source: "image://theme/icon-l-play"
                onClicked: root.playPauseClicked()
            }

            IconButton {
                id: nextButton
                icon.source: "image://theme/icon-m-next"
                onClicked: root.nextClicked()
            }
        }
    }

    states: [
        State {
            name: "stopped"
            PropertyChanges {
                target: playPauseButton
                icon.source: "image://theme/icon-l-play"
            }
            PropertyChanges {
                target: seekbar
                valueText: ""
            }
        },

        State {
            name: "paused"
            PropertyChanges {
                target: playPauseButton
                icon.source: "image://theme/icon-l-play"
            }
            PropertyChanges {
                target: seekbar
                valueText: seekPossible ? msToMinutesAndSeconds(playbackPosition) : ""
            }
        },

        State {
            name: "playing"
            PropertyChanges {
                target: playPauseButton
                icon.source: "image://theme/icon-l-pause"
            }
            PropertyChanges {
                target: seekbar
                valueText: seekPossible ? msToMinutesAndSeconds(playbackPosition) : ""
            }
        }

    ]
}
