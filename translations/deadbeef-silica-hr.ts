<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hr">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/dialogs/About.qml" line="27"/>
        <source>About</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="42"/>
        <source>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 This program is licensed under the terms of GPLv3 license
                                 
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues                                  tracker to file bug reports and feature requests.
                                 
                                 Credits:
                                 
                                 %3</source>
        <translation>DeadBeef-Silica dodatak za sučelja %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 Ovaj je program licenciran pod uvjetima GPLv3 licence
                                 
                                 Koristi https://bitbucket.org/kravich/deadbeef-silica/issues                                  za prijavljivanje grešaka i predlaganje novih funkcija.
                                 
                                 Zahvale:
                                 
                                 %3</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="53"/>
        <source>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Russian translation</source>
        <translation>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Ruski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="59"/>
        <source>Allan Nordhøy
                                                    Norwegian Bokmål translation</source>
        <translation>Allan Nordhøy
                                                    Norveški Bokmål prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="67"/>
        <source>Rui Kon
                                                    Elizabeth Sherrock
                                                    Chinese translation</source>
        <translation>Rui Kon
                                                    Elizabeth Sherrock
                                                    Kineski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="70"/>
        <source>Szabó G
                                                    Hungarian translation</source>
        <translation>Szabó G
                                                    Mađarski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="56"/>
        <source>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Spanish translation</source>
        <translation>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Španjolski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="61"/>
        <source>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    French translation</source>
        <translation>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    Francuski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="72"/>
        <source>O S
                                                    J. Lavoie
                                                    German translation</source>
        <translation>O S
                                                    J. Lavoie
                                                    Njemački prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="75"/>
        <source>Åke Engelbrektson
                                                     Swedish translation</source>
        <translation>Åke Engelbrektson
                                                     Švedski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="77"/>
        <source>Reima Vesterinen
                                                     Finnish translation</source>
        <translation>Reima Vesterinen
                                                     Finski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="79"/>
        <source>Milo Ivir
                                                     Croatian translation</source>
        <translation>Milo Ivir
                                                     Hrvatski prijevod</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="120"/>
        <source>DeadBeef License</source>
        <translation>DeadBeef licenca</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="131"/>
        <source>DeadBeef is licensed under the terms of zlib license. For more information please refer to %1</source>
        <translation>DeadBeef je licenciran pod uvjetima zlib licence. Za više informacija pogledajte %1</translation>
    </message>
</context>
<context>
    <name>AddLocation</name>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="21"/>
        <source>Add location</source>
        <translation>Dodaj mjesto</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="41"/>
        <source>Location (URI or file path)</source>
        <translation>Mjesto (URI ili staza datoteke)</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>kHz</source>
        <translation>kHz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="20"/>
        <source>Zero all</source>
        <translation>Sve na nulu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="47"/>
        <source>Equalizer</source>
        <translation>Ekvilajzer</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="59"/>
        <source>Enable</source>
        <translation>Aktiviraj</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="109"/>
        <source>Preamp</source>
        <translation>Pretpojačalo</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>+%1 dB</source>
        <translation>+%1 dB</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="55"/>
        <source>Hide system files</source>
        <translation>Sakrij sustavske datoteke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="56"/>
        <source>Show system files</source>
        <translation>Prikaži sustavske datoteke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="65"/>
        <source>Select current dir</source>
        <translation>Odaberi trenutačnu mapu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="66"/>
        <source>Select</source>
        <translation>Odaberi</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="84"/>
        <source>Filter</source>
        <translation>Filtar</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="108"/>
        <source>Path</source>
        <translation>Staza</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="13"/>
        <source>untitled</source>
        <comment>default name of the file to be saved</comment>
        <translation>bez naslova</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="106"/>
        <source>Location</source>
        <translation>Mjesto</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="130"/>
        <source>Select file location</source>
        <translation>Odaberi mjesto datoteke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="145"/>
        <source>New file name</source>
        <translation>Ime nove datoteke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="163"/>
        <source>File exists. Overwrite?</source>
        <translation>Datoteka postoji. Prepisati?</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="178"/>
        <source>There is already a directory with such name</source>
        <translation>Mapa s tim imenom već postoji</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="194"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
</context>
<context>
    <name>FileSelectionField</name>
    <message>
        <location filename="../qml/items/FileSelectionField.qml" line="39"/>
        <source>Select %1</source>
        <comment>title of dialog that selects path to file in plugin settings</comment>
        <translation>Odaberi %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <source>Supported formats</source>
        <translation>Podržani formati</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <source>All files</source>
        <translation>Sve datoteke</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>Supported playlist formats</source>
        <translation>Podržani formati playlista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Open file(s)/folder(s)</source>
        <comment>item of pull-up menu</comment>
        <translation>Otvori datoteke/mape</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Open file(s)/folder(s)</source>
        <comment>title of dialog page</comment>
        <translation>Otvori datoteke/mape</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="105"/>
        <source>Load playlist</source>
        <comment>item of pull-up menu</comment>
        <translation>Učitaj playlistu</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="109"/>
        <source>Load playlist</source>
        <comment>title of dialog page</comment>
        <translation>Učitaj playlistu</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>More…</source>
        <translation>Više …</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>Empty playlist</source>
        <translation>Prazna playlista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="137"/>
        <source>Pull down to add files</source>
        <translation>Za dodavanje datoteka, povuci dolje</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="203"/>
        <source>Add To Playback Queue</source>
        <translation>Dodaj u red sviranja</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="208"/>
        <source>Remove From Playback Queue</source>
        <translation>Ukloni iz reda sviranja</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="216"/>
        <source>Track Properties</source>
        <translation>Svojstva pjesme</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="222"/>
        <source>Delete</source>
        <translation>Izbriši</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="229"/>
        <source>Deleting</source>
        <translation>Brisanje</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>Equalizer</source>
        <translation>Ekvilajzer</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="272"/>
        <source>Settings</source>
        <translation>Postavke</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="277"/>
        <source>About</source>
        <translation>Informacije</translation>
    </message>
</context>
<context>
    <name>PlaylistActions</name>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="30"/>
        <source>Supported formats</source>
        <translation>Podržani formati</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="31"/>
        <source>All files</source>
        <translation>Sve datoteke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="70"/>
        <source>Playlist actions</source>
        <translation>Radnje za playlistu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="77"/>
        <source>Add file(s)/folder(s)</source>
        <comment>playlist actions menu item</comment>
        <translation>Dodaj datoteke/mape</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="79"/>
        <source>Save playlist</source>
        <comment>playlist actions menu item</comment>
        <translation>Spremi playlistu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="90"/>
        <source>Add file(s)/folder(s)</source>
        <comment>file add dialog title</comment>
        <translation>Dodaj datoteke/mape</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="108"/>
        <source>Save playlist</source>
        <comment>save playlist dialog title</comment>
        <translation>Spremi playlistu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="78"/>
        <source>Add location</source>
        <translation>Dodaj mjesto</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="80"/>
        <source>Sort playlist</source>
        <translation>Razvrstaj playlistu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="81"/>
        <source>Clear playlist</source>
        <translation>Isprazni playlistu</translation>
    </message>
</context>
<context>
    <name>PlaylistNameEdit</name>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Add playlist</source>
        <translation>Dodaj playlistu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Rename playlist</source>
        <translation>Preimenuj playlistu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="42"/>
        <source>Playlist name</source>
        <translation>Ime playliste</translation>
    </message>
</context>
<context>
    <name>PlaylistSorting</name>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="16"/>
        <source>Sort by</source>
        <translation>Redoslijed</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="29"/>
        <source>Reverse order</source>
        <translation>Preokreni redoslijed</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="45"/>
        <source>Artist/Album/Track number</source>
        <translation>Izvođač/album/broj pjesme</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="46"/>
        <source>Artist/Album</source>
        <translation>Izvođač/Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="47"/>
        <source>Title</source>
        <translation>Naslov</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="48"/>
        <source>Track number</source>
        <translation>Broj pjesme</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="49"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="50"/>
        <source>Artist</source>
        <translation>Izvođač</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="51"/>
        <source>Album artist</source>
        <translation>Izvođač albuma</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="52"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="53"/>
        <source>Filename</source>
        <translation>Ime datoteke</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="40"/>
        <source>New playlist</source>
        <translation>Nova playlista</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="19"/>
        <source>Add new playlist</source>
        <translation>Dodaj novu playlistu</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="67"/>
        <source>Playlists</source>
        <translation>Playliste</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="94"/>
        <source>Rename</source>
        <translation>Preimenuj</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="114"/>
        <source>Delete</source>
        <translation>Izbriši</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="121"/>
        <source>Deleting</source>
        <translation>Brisanje</translation>
    </message>
</context>
<context>
    <name>PluginCopyright</name>
    <message>
        <location filename="../qml/dialogs/PluginCopyright.qml" line="24"/>
        <source>Copyright</source>
        <translation>Autorska prava</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="29"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="53"/>
        <source>Version</source>
        <translation>Verzija</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="59"/>
        <source>Website</source>
        <translation>Web-stranica</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="82"/>
        <source>Settings</source>
        <translation>Postavke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="90"/>
        <source>Copyright</source>
        <translation>Autorska prava</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="31"/>
        <source>plays aac files, supports raw aac files, as well as mp4 container</source>
        <translation>svira aac datoteke, podržava osnovne aac datoteke, kao i mp4 kontejner</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="35"/>
        <source>Adplug player (ADLIB OPL2/OPL3 emulator)</source>
        <translation>Adplug player (ADLIB OPL2/OPL3 emulator)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="41"/>
        <source>plays alac files from MP4 and M4A files</source>
        <translation>svira alac datoteke od MP4 i M4A datoteka</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="45"/>
        <source>plays psf, psf2, spu, ssf, dsf, qsf file formats</source>
        <translation>svira datotečne formate psf, psf2, spu, ssf, dsf, qsf</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="49"/>
        <source>Loads album artwork from embedded tags, local directories, or internet services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="63"/>
        <source>plays dts-encoded files using libdca from VLC project</source>
        <translation>svira dts-kodirane datoteke pomoću libdca iz VLC projekta</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="67"/>
        <source>User interface implemented using Sailfish Silica QML module</source>
        <translation>Implementirano korisničko sučelje pomoću Sailfish Silica QML modula</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="71"/>
        <source>High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/</source>
        <translation>Visokokvalitetni pretvarač uzorka pomoću libsamplerata, http://www.mega-nerd.com/SRC/</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="75"/>
        <source>module player based on DUMB library</source>
        <translation>modulski player baziran na DUMB biblioteci</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="83"/>
        <source>APE player based on code from libavc and rockbox</source>
        <translation>APE player baziran na kodu od libavc i rockbox</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="87"/>
        <source>decodes audio formats using FFMPEG libavcodec</source>
        <translation>dekodira audio formate koristeći FFMPEG libavcodec</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="93"/>
        <source>FLAC decoder using libFLAC</source>
        <translation>FLAC dekoder pomoću libFLAC</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="99"/>
        <source>chiptune/game music player based on GME library</source>
        <translation>Glazbeni player za chiptune/igre baziran na GME biblioteci</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="107"/>
        <source>Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol</source>
        <translation>Šalje podatke o sviranim pjesmama na tvoj last.fm račun ili drugu uslugu koja koristi AudioScrobbler protokol</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="119"/>
        <source>Importing and exporting M3U and PLS formats
Recognizes .pls, .m3u and .m3u8 file types

NOTE: only utf8 file names are currently supported</source>
        <translation>Uvoz i izvoz M3U i PLS formata
Prepoznaje .pls, .m3u i .m3u8 datoteke

NAPOMENA: imena datoteka su trenutačno podržana samo u utf8 formatu</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="123"/>
        <source>MMS streaming plugin based on libmms</source>
        <translation>Priključak za MMS prijenos baziran na libmms</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="127"/>
        <source>Mono to stereo converter DSP</source>
        <translation>Mono u stereo konverter DSP</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="131"/>
        <source>MPEG v1/2 layer1/2/3 decoder

Using libmpg123 backend.
</source>
        <translation>MPEG v1/2 layer1/2/3 dekoder

Koristi pozadinski program libmpg123.
</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="137"/>
        <source>Communicate with other applications using D-Bus.</source>
        <translation>Komuniciraj s drugim programima pomoću D-busa.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="141"/>
        <source>Musepack decoder using libmppdec</source>
        <translation>Musepack dekoder pomoću libmppdec</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="145"/>
        <source>This plugin takes the audio data, and discards it,
so nothing will play.
This is useful for testing.</source>
        <translation>Ovaj dodatak uzima audio podatke i odbacuje ih,
tako da ništa neće svirati.
Ovo je korisno za testiranje.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="149"/>
        <source>Opus player based on libogg, libopus and libopusfile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="153"/>
        <source>At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.
If you experience problems - please try switching to ALSA or OSS output.
If that doesn&apos;t help - please uninstall PulseAudio from your system, and try ALSA or OSS again.
Thanks for understanding</source>
        <translation>U trenutku pisanja ovog teksta čini se da je PulseAudio vrlo nestabilan u mnogim (ili većini) GNU/Linux distribucijama.
Ako naiđeš na probleme – pokušaj prebaciti na ALSA ili OSS izlaz.
Ako to ne pomogne – deinstaliraj PulseAudio sa svog sustava i ponovo pokušaj ALSA ili OSS.
Hvala na razumijevanju</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="159"/>
        <source>SC68 player (Atari ST SNDH YM2149)</source>
        <translation>SC68 player (Atari ST SNDH YM2149)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="166"/>
        <source>decodes shn files</source>
        <translation>dekodira shn datoteke</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="173"/>
        <source>SID player based on libsidplay2</source>
        <translation>SID player baziran na libsidplay2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="183"/>
        <source>wav/aiff player using libsndfile</source>
        <translation>wav/aiff player pomoću libsndfile</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="188"/>
        <source>Tempo/Pitch/Rate changer using SoundTouch Library (http://www.surina.net/soundtouch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="192"/>
        <source>equalizer plugin using SuperEQ library</source>
        <translation>dodatak za ekvilajzer koji koristi SuperEQ biblioteku</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="196"/>
        <source>tta decoder based on TTA Hardware Players Library Version 1.2</source>
        <translation>tta dekoder baziran na TTA Hardware Players Library Version 1.2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="200"/>
        <source>http and ftp streaming module using libcurl, with ICY protocol support</source>
        <translation>Modul za http i ftp prijenos pomoću libcurl, s podrškom za ICY protokol</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="205"/>
        <source>Standard IO plugin
Used for reading normal local files
It is statically linked, so you can&apos;t delete it.</source>
        <translation>Standardni dodatak za ulaz-izlaz (IO)
Koristi se za čitanje normalnih lokalnih datoteka
Ne može se izbrisati.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="209"/>
        <source>play files directly from zip files</source>
        <translation>sviraj datoteke izravno iz zip-datoteka</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="213"/>
        <source>Ogg Vorbis decoder using standard xiph.org libraries</source>
        <translation>Ogg Vorbis dekoder koji koristi standardne xiph.org biblioteke</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="217"/>
        <source>AY8910/12 chip emulator and vtx file player</source>
        <translation>Emulator AY8910/12 čipa i player vtx datoteka</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="222"/>
        <source>WavPack (.wv, .iso.wv) player</source>
        <translation>WavPack (.wv, .iso.wv) player</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="226"/>
        <source>MIDI player based on WildMidi library

Requires freepats package to be installed
See http://freepats.zenvoid.org/
Make sure to set correct freepats.cfg path in plugin settings.</source>
        <translation>MIDI player baziran na WildMidi biblioteci

Za instaliranje je potreban paket freepats
Pogledaj http://freepats.zenvoid.org/
Obavezno postavi ispravnu stazu freepats.cfg u postavkama dodatka.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="231"/>
        <source>plays WMA files</source>
        <translation>svira WMA datoteke</translation>
    </message>
</context>
<context>
    <name>PluginSettings</name>
    <message>
        <location filename="../qml/dialogs/PluginSettings.qml" line="15"/>
        <source>%1 settings</source>
        <translation>%1 postavke</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="108"/>
        <source>Enable scrobbler</source>
        <translation>Aktiviraj scrobbler</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="109"/>
        <source>Disable nowplaying</source>
        <translation>Ne obavještavaj što upravo svira</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="110"/>
        <source>Username</source>
        <translation>Korisničko ime</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="111"/>
        <source>Password</source>
        <translation>Lozinka</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="112"/>
        <source>Scrobble URL</source>
        <translation>Scrobble URL</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="113"/>
        <source>Prefer Album Artist over Artist field</source>
        <translation>Preferiraj „Izvođač albuma” umjesto polja „Izvođač”</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="114"/>
        <source>Send MusicBrainz ID</source>
        <translation>Pošalji MusicBrainz ID</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="115"/>
        <source>Submit tracks shorter than 30 seconds (not recommended)</source>
        <translation>Pošalji pjesme kraće od 30 sekundi (ne preporučuje se)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="100"/>
        <source>Max song length (in minutes)</source>
        <translation>Maks. trajanje pjesme (u minutama)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="101"/>
        <source>Fadeout length (seconds)</source>
        <translation>Trajanje postupnog prijelaza (sekunde)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="102"/>
        <source>Play loops nr. of times (if available)</source>
        <translation>Ponavljaj ovoliki broj puta (ako je dostupno)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="155"/>
        <source>Preferred buffer size</source>
        <translation>Preferirana veličina međuspremnika</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="167"/>
        <source>Relative seek table path</source>
        <translation>Relativna staza tablice traženja</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="168"/>
        <source>Absolute seek table path</source>
        <translation>Apsolutna staza tablice traženja</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="169"/>
        <source>Swap audio bytes (toggle if all you hear is static)</source>
        <translation>Zamijeni audio bajtove (uklj/isklj ako čuješ samo šum)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="174"/>
        <source>Enable HVSC Songlength DB</source>
        <translation>Aktiviraj HVSC Songlength DB</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="161"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="176"/>
        <source>Samplerate</source>
        <translation>Frekvencija</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="178"/>
        <source>Mono synth</source>
        <translation>Mono sintersajzer</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="179"/>
        <source>Default song length (sec)</source>
        <translation>Standardno trajanje pjesme (sekunde)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="201"/>
        <source>Enable logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="227"/>
        <source>Timidity++ bank configuration file</source>
        <translation>Konfiguracijska datoteka za Timidity++</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="88"/>
        <source>Use all extensions supported by ffmpeg</source>
        <translation>Koristi sve nastavke koje ffmpeg podržava</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="89"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="184"/>
        <source>File Extensions (separate with &apos;;&apos;)</source>
        <translation>Datotečni nastavci (odvojeni s točka-zarezom)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="132"/>
        <source>Backend</source>
        <translation>Pozadinski program</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="36"/>
        <source>Prefer Ken emu over Satoh (surround won&apos;t work)</source>
        <translation>Preferiraj Ken emu nad Satoh (surround funkcija neće raditi)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="37"/>
        <source>Enable surround</source>
        <translation>Aktiviraj surround ozvučenje</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="50"/>
        <source>Cache update period (in hours, 0=never)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="51"/>
        <source>Fetch from embedded tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="52"/>
        <source>Fetch from local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="53"/>
        <source>Fetch from Last.fm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="54"/>
        <source>Fetch from MusicBrainz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="55"/>
        <source>Fetch from Albumart.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="56"/>
        <source>Fetch from World of Spectrum (AY files only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="57"/>
        <source>When no artwork is found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="58"/>
        <source>Custom image path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="59"/>
        <source>Scale artwork towards longer side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="76"/>
        <source>Resampling quality</source>
        <translation>Kvaliteta preuzorkovanja</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="77"/>
        <source>8-bit output</source>
        <translation>8-bitni izlaz</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="78"/>
        <source>Internal DUMB volume</source>
        <translation>Interna DUMB glasnoća</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="79"/>
        <source>Volume ramping</source>
        <translation>Pojačavanje zvuka</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="94"/>
        <source>Ignore bad header errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="95"/>
        <source>Ignore unparsable stream errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="103"/>
        <source>ColecoVision BIOS (for SGC file format)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="133"/>
        <source>Force 16 bit output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="154"/>
        <source>PulseAudio server (leave empty for default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="160"/>
        <source>Default song length (in minutes)</source>
        <translation>Standardno trajanje pjesme (u minutama)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="162"/>
        <source>Skip when shorter than (sec)</source>
        <translation>Preskoči kad je kraća od (sekunde)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="175"/>
        <source>Full path to Songlengths.md5/.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="177"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="218"/>
        <source>Bits per sample</source>
        <translation>Bitova po uzorku</translation>
    </message>
</context>
<context>
    <name>Plugins</name>
    <message>
        <location filename="../qml/dialogs/Plugins.qml" line="11"/>
        <source>Plugins</source>
        <translation>Dodaci</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation>Postavke</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="29"/>
        <source>Playback options</source>
        <translation>Opcije za sviranje</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="33"/>
        <source>Looping</source>
        <translation>Ponavljanje</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="42"/>
        <source>Don&apos;t loop</source>
        <translation>Ne ponavljaj</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="48"/>
        <source>Loop single song</source>
        <translation>Ponavljaj jednu pjesmu</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="54"/>
        <source>Loop all</source>
        <translation>Ponavljaj sve</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="61"/>
        <source>Playback order</source>
        <translation>Redoslijed sviranja</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="71"/>
        <source>Linear</source>
        <translation>Linearno</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="77"/>
        <source>Shuffle tracks</source>
        <translation>Biraj pjesme nasumice</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="83"/>
        <source>Shuffle albums</source>
        <translation>Biraj albume nasumice</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="89"/>
        <source>Random</source>
        <translation>Nasumice</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="97"/>
        <source>Resume previous session on startup</source>
        <translation>Nastavi prethodnu sesiju nakon pokretanja</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="112"/>
        <source>Pause playback when headset is unplugged</source>
        <translation>Zaustavi sviranje kad se slušalice odspoje</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="119"/>
        <source>Resume playback when headset is plugged</source>
        <translation>Nastavi sviranje kad se slušalice priključe</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="126"/>
        <source>View options</source>
        <translation>Pogledaj opcije</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="139"/>
        <source>Playlist grouping</source>
        <translation>Grupiranje playliste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="149"/>
        <source>None</source>
        <translation>Ništa</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="155"/>
        <source>Artist/Date/Album</source>
        <translation>Izvođač/datum/album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="161"/>
        <source>Artist</source>
        <translation>Izvođač</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="167"/>
        <source>Custom</source>
        <translation>Prilagođeno</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="182"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="197"/>
        <source>Please use &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; spec as a reference on format fields for grouping</source>
        <translation>Koristi &lt;a href=&quot;%1&quot;&gt;ovu&lt;/a&gt; specifikaciju kao referencu o poljima formata za grupiranje</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="130"/>
        <source>Use simple one-line playlist items</source>
        <translation>Koristi jednoredne elemente playliste</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="211"/>
        <source>Display album art for artist/album groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="218"/>
        <source>Display album art on cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="251"/>
        <source>Setup Last.fm</source>
        <translation>Postavi Last.fm</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="261"/>
        <source>Plugins</source>
        <translation>Dodaci</translation>
    </message>
</context>
<context>
    <name>TrackProperties</name>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="28"/>
        <source>Track Properties</source>
        <translation>Svojstva pjesme</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="45"/>
        <source>Metadata</source>
        <translation>Metapodaci</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="49"/>
        <source>No metadata</source>
        <translation>Nema metapodataka</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="67"/>
        <source>Properties</source>
        <translation>Svojstva</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="71"/>
        <source>No properties</source>
        <translation>Nema svojstva</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="8"/>
        <source>Artist</source>
        <translation>Izvođač</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="9"/>
        <source>Track Title</source>
        <translation>Naslov pjesme</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="10"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="11"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="12"/>
        <source>Track Number</source>
        <translation>Broj pjesme</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="13"/>
        <source>Total Tracks</source>
        <translation>Ukupno pjesama</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="14"/>
        <source>Genre</source>
        <translation>Žanr</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="15"/>
        <source>Composer</source>
        <translation>Skladatelj</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="16"/>
        <source>Disc Number</source>
        <translation>Broj diska</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="17"/>
        <source>Total Disks</source>
        <translation>Ukupno diskova</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="18"/>
        <source>Comment</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="22"/>
        <source>Location</source>
        <translation>Mjesto</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="23"/>
        <source>Subtrack Index</source>
        <translation>Indeks pod-pjesama</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="24"/>
        <source>Duration</source>
        <translation>Trajanje</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="25"/>
        <source>Tag Type(s)</source>
        <translation>Vrste oznaka</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="26"/>
        <source>Embedded Cuesheet</source>
        <translation>Ugrađeni red</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="27"/>
        <source>Codec</source>
        <translation>Kodek</translation>
    </message>
</context>
</TS>
