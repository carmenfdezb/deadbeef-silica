<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/dialogs/About.qml" line="27"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="42"/>
        <source>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 This program is licensed under the terms of GPLv3 license
                                 
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues                                  tracker to file bug reports and feature requests.
                                 
                                 Credits:
                                 
                                 %3</source>
        <translation>DeadBeef-Silica GUI plugin %1
                                 Copyright © %2 Evgeny Kravchenko &lt;cravchik@yandex.ru&gt;
                                 Dieses Programm ist unter den Bedingungen der GPLv3 lizensiert
                                 
                                 Bittte benutzen Sie https://bitbucket.org/kravich/deadbeef-silica/issues                                  um Bugs zu melden oder Verbesserungen vorzuschlagen.
                                 
                                 Credits:
                                 
                                 %3</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="53"/>
        <source>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Russian translation</source>
        <translation>Vyacheslav Dikonov &lt;sdiconov@mail.ru&gt;
                                                    Nikolai Sinyov
                                                    Russische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="59"/>
        <source>Allan Nordhøy
                                                    Norwegian Bokmål translation</source>
        <translation>Allan Nordhøy
                                                    Norwegische (Bokmål) Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="67"/>
        <source>Rui Kon
                                                    Elizabeth Sherrock
                                                    Chinese translation</source>
        <translation>Rui Kon
                                                    Elizabeth Sherrock
                                                    Chinesische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="70"/>
        <source>Szabó G
                                                    Hungarian translation</source>
        <translation>Szabó G
                                                    Ungarische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="56"/>
        <source>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Spanish translation</source>
        <translation>Carmen Fdez. B.
                                                    J. Lavoie
                                                    Spanische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="61"/>
        <source>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    French translation</source>
        <translation>Quenti
                                                    objectifnul
                                                    ButterflyOfFire
                                                    Nathan
                                                    J. Lavoie
                                                    Französiche Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="72"/>
        <source>O S
                                                    J. Lavoie
                                                    German translation</source>
        <translation>O S
                                                    J. Lavoie
                                                    Deutsche Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="75"/>
        <source>Åke Engelbrektson
                                                     Swedish translation</source>
        <translation>Åke Engelbrektson
                                                     Schwedische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="77"/>
        <source>Reima Vesterinen
                                                     Finnish translation</source>
        <translation>Reima Vesterinen
                                                     Finnische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="79"/>
        <source>Milo Ivir
                                                     Croatian translation</source>
        <translation>Milo Ivir
                                                     Kroatische Übersetzung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="120"/>
        <source>DeadBeef License</source>
        <translation>DeadBeef Lizenz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/About.qml" line="131"/>
        <source>DeadBeef is licensed under the terms of zlib license. For more information please refer to %1</source>
        <translation>DeadBeef ist unter den Bedingungen der zlib-Lizenz lizenziert. Weitere Informationen finden Sie unter %1</translation>
    </message>
</context>
<context>
    <name>AddLocation</name>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="21"/>
        <source>Add location</source>
        <translation>Ort hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/AddLocation.qml" line="41"/>
        <source>Location (URI or file path)</source>
        <translation>Ort (URL oder Dateipfad)</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="13"/>
        <source>kHz</source>
        <translation>kHz</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="20"/>
        <source>Zero all</source>
        <translation>Alle zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="47"/>
        <source>Equalizer</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="59"/>
        <source>Enable</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="109"/>
        <source>Preamp</source>
        <translation>Vorverstärker</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>+%1 dB</source>
        <translation>+%1 dB</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Equalizer.qml" line="137"/>
        <location filename="../qml/dialogs/Equalizer.qml" line="315"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="55"/>
        <source>Hide system files</source>
        <translation>System-Dateien verstecken</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="56"/>
        <source>Show system files</source>
        <translation>System-Dateien zeigen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="65"/>
        <source>Select current dir</source>
        <translation>Aktuelles Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="66"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="84"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FilePicker.qml" line="108"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="13"/>
        <source>untitled</source>
        <comment>default name of the file to be saved</comment>
        <translation>ohne Titel</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="106"/>
        <source>Location</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="130"/>
        <source>Select file location</source>
        <translation>Dateispeicherort wählen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="145"/>
        <source>New file name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="163"/>
        <source>File exists. Overwrite?</source>
        <translation>Datei ist vorhanden. Überschreiben?</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="178"/>
        <source>There is already a directory with such name</source>
        <translation>Ein Verzeichnis mit diesem Namen existiert bereits</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/FileSaveDialog.qml" line="194"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
</context>
<context>
    <name>FileSelectionField</name>
    <message>
        <location filename="../qml/items/FileSelectionField.qml" line="39"/>
        <source>Select %1</source>
        <comment>title of dialog that selects path to file in plugin settings</comment>
        <translation>Wählen Sie %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <source>Supported formats</source>
        <translation>Unterstützte Formate</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="39"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>Supported playlist formats</source>
        <translation>Unterstützte Formate</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Open file(s)/folder(s)</source>
        <comment>item of pull-up menu</comment>
        <translation>Datei(en)/Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Open file(s)/folder(s)</source>
        <comment>title of dialog page</comment>
        <translation>Datei(en)/Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="105"/>
        <source>Load playlist</source>
        <comment>item of pull-up menu</comment>
        <translation>Playlist öffnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="109"/>
        <source>Load playlist</source>
        <comment>title of dialog page</comment>
        <translation>Playlist öffnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>More…</source>
        <translation>Hinzufügen…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>Empty playlist</source>
        <translation>Leere Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="137"/>
        <source>Pull down to add files</source>
        <translation>Nach unten ziehen, um Dateien hinzuzufügen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="203"/>
        <source>Add To Playback Queue</source>
        <translation>Zur Warteschlange hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="208"/>
        <source>Remove From Playback Queue</source>
        <translation>Aus der Warteschlange entfernen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="216"/>
        <source>Track Properties</source>
        <translation>Track-Eigenschaften</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="222"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="229"/>
        <source>Deleting</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>Equalizer</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="272"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="277"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>PlaylistActions</name>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="30"/>
        <source>Supported formats</source>
        <translation>Unterstützte Formate</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="31"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="70"/>
        <source>Playlist actions</source>
        <translation>Playlist-Optionen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="77"/>
        <source>Add file(s)/folder(s)</source>
        <comment>playlist actions menu item</comment>
        <translation>Datei(en)/Ordner hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="79"/>
        <source>Save playlist</source>
        <comment>playlist actions menu item</comment>
        <translation>Playlist speichern</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="90"/>
        <source>Add file(s)/folder(s)</source>
        <comment>file add dialog title</comment>
        <translation>Datei(en)/Ordner hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="108"/>
        <source>Save playlist</source>
        <comment>save playlist dialog title</comment>
        <translation>Playlist speichern</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="78"/>
        <source>Add location</source>
        <translation>Ort hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="80"/>
        <source>Sort playlist</source>
        <translation>Playlist sortieren</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistActions.qml" line="81"/>
        <source>Clear playlist</source>
        <translation>Playlist leeren</translation>
    </message>
</context>
<context>
    <name>PlaylistNameEdit</name>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Add playlist</source>
        <translation>Playlist hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="22"/>
        <source>Rename playlist</source>
        <translation>Playlist umbenennen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistNameEdit.qml" line="42"/>
        <source>Playlist name</source>
        <translation>Playlist-Name</translation>
    </message>
</context>
<context>
    <name>PlaylistSorting</name>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="16"/>
        <source>Sort by</source>
        <translation>Sortieren nach</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="29"/>
        <source>Reverse order</source>
        <translation>Reihenfolge umkehren</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="45"/>
        <source>Artist/Album/Track number</source>
        <translation>Künstler/Album/Track-Nummer</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="46"/>
        <source>Artist/Album</source>
        <translation>Künstler/Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="47"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="48"/>
        <source>Track number</source>
        <translation>Track-Nummer</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="49"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="50"/>
        <source>Artist</source>
        <translation>Künstler</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="51"/>
        <source>Album artist</source>
        <translation>Album-Künstler</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="52"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PlaylistSorting.qml" line="53"/>
        <source>Filename</source>
        <translation>Dateiname</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="40"/>
        <source>New playlist</source>
        <translation>Neue Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="19"/>
        <source>Add new playlist</source>
        <translation>Neue Playlist erstellen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="67"/>
        <source>Playlists</source>
        <translation>Playlisten</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="94"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="114"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PlaylistsPage.qml" line="121"/>
        <source>Deleting</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>PluginCopyright</name>
    <message>
        <location filename="../qml/dialogs/PluginCopyright.qml" line="24"/>
        <source>Copyright</source>
        <translation>Urheberrecht</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="29"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="53"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="59"/>
        <source>Website</source>
        <translation>Website</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="82"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/PluginInfo.qml" line="90"/>
        <source>Copyright</source>
        <translation>Urheberrecht</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="31"/>
        <source>plays aac files, supports raw aac files, as well as mp4 container</source>
        <translation>Spielt aac-Dateien, unterstützt raw-aac-Dateien, sowie mp4-Container</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="35"/>
        <source>Adplug player (ADLIB OPL2/OPL3 emulator)</source>
        <translation>AdPlug-Player (ADLIB OPL2/OPL3-Emulator)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="41"/>
        <source>plays alac files from MP4 and M4A files</source>
        <translation>Spielt alac-Dateien aus mp4- und m4a-Dateien</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="45"/>
        <source>plays psf, psf2, spu, ssf, dsf, qsf file formats</source>
        <translation>Spielt psf-, psf2-, spu- ssf-, dsf- und qsf-Dateien</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="49"/>
        <source>Loads album artwork from embedded tags, local directories, or internet services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="63"/>
        <source>plays dts-encoded files using libdca from VLC project</source>
        <translation>Spielt dts-codierte Dateien mit libdca aus dem VLC-Projekt</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="67"/>
        <source>User interface implemented using Sailfish Silica QML module</source>
        <translation>Benutzeroberfläche für Sailfish mit dem Silica QML-Modul implementiert</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="71"/>
        <source>High quality samplerate converter using libsamplerate, http://www.mega-nerd.com/SRC/</source>
        <translation>Hochwertiger Sampleraten-Konverter auf Basis von libsamplerate, http://www.mega-nerd.com/SRC/</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="75"/>
        <source>module player based on DUMB library</source>
        <translation>Modul-Player basierend auf der DUMB-Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="83"/>
        <source>APE player based on code from libavc and rockbox</source>
        <translation>APE-Player basierend auf Code von libavc und rockbox</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="87"/>
        <source>decodes audio formats using FFMPEG libavcodec</source>
        <translation>Dekodiert Audio-Formate mit FFMPEG libavodec</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="93"/>
        <source>FLAC decoder using libFLAC</source>
        <translation>FLAC-Decoder mit libFLAC</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="99"/>
        <source>chiptune/game music player based on GME library</source>
        <translation>Chiptune/Game Music Player basierend auf GME-Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="107"/>
        <source>Sends played songs information to your last.fm account, or other service that use AudioScrobbler protocol</source>
        <translation>Sendet Informationen zu abgespielten Songs an Ihr last.fm-Konto oder einen anderen Dienst, der das AudioScrobbler-Protokoll verwendet</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="119"/>
        <source>Importing and exporting M3U and PLS formats
Recognizes .pls, .m3u and .m3u8 file types

NOTE: only utf8 file names are currently supported</source>
        <translation>Import und Export von M3U- und PLS-Formaten
Erkennt .pls-, .m3u- und .m3u8-Dateien

Achtung: Nur utf8-Dateinamen werden derzeit unterstützt</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="123"/>
        <source>MMS streaming plugin based on libmms</source>
        <translation>MMS-Streaming-Plugin basierend auf libmms</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="127"/>
        <source>Mono to stereo converter DSP</source>
        <translation>Mono-Stereo-Wandler DSP</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="131"/>
        <source>MPEG v1/2 layer1/2/3 decoder

Using libmpg123 backend.
</source>
        <translation>MPEG v1/2 Layer1/2/3 Dekodierer

Verwendung von libmpg123 Backend.
</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="137"/>
        <source>Communicate with other applications using D-Bus.</source>
        <translation>Kommunikation mit anderen Anwendungen über D-Bus.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="141"/>
        <source>Musepack decoder using libmppdec</source>
        <translation>Musepack-Decoder mit libmppdec</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="145"/>
        <source>This plugin takes the audio data, and discards it,
so nothing will play.
This is useful for testing.</source>
        <translation>Dieses Plugin nimmt die Audiodaten und verwirft sie,
so wird nichts abgespielt.
Dies ist nützlich für Tests.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="149"/>
        <source>Opus player based on libogg, libopus and libopusfile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="153"/>
        <source>At the moment of this writing, PulseAudio seems to be very unstable in many (or most) GNU/Linux distributions.
If you experience problems - please try switching to ALSA or OSS output.
If that doesn&apos;t help - please uninstall PulseAudio from your system, and try ALSA or OSS again.
Thanks for understanding</source>
        <translation>Im Moment dieses Schreibens scheint PulseAudio in vielen (oder den meisten) GNU/Linux-Distributionen sehr instabil zu sein.
Wenn Sie Probleme haben- versuchen Sie bitte, auf ALSA- oder OSS-Ausgang umzuschalten.
Wenn das nicht hilft - deinstallieren Sie bitte PulseAudio von Ihrem System, und versuchen Sie es erneut mit ALSA oder OSS.
Vielen Dank für Ihr Verständnis</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="159"/>
        <source>SC68 player (Atari ST SNDH YM2149)</source>
        <translation>SC68-Spieler (Atari ST SNDH YM2149)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="166"/>
        <source>decodes shn files</source>
        <translation>Dekodiert shn-Dateien</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="173"/>
        <source>SID player based on libsidplay2</source>
        <translation>SID-Spieler auf Basis von libsidplay2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="183"/>
        <source>wav/aiff player using libsndfile</source>
        <translation>Spielt wav- und aiff-Dateien mit libsndfile</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="188"/>
        <source>Tempo/Pitch/Rate changer using SoundTouch Library (http://www.surina.net/soundtouch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="192"/>
        <source>equalizer plugin using SuperEQ library</source>
        <translation>Equalizer-Plugin mit der SuperEQ-Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="196"/>
        <source>tta decoder based on TTA Hardware Players Library Version 1.2</source>
        <translation>tta-Decoder basierend auf der TTA Hardware Players Library Version 1.2</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="200"/>
        <source>http and ftp streaming module using libcurl, with ICY protocol support</source>
        <translation>HTTP- und FTP-Streaming-Modul mit libcurl und ICY-Protokollunterstützung</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="205"/>
        <source>Standard IO plugin
Used for reading normal local files
It is statically linked, so you can&apos;t delete it.</source>
        <translation>Standard IO Plugin
Wird zum Lesen normaler lokaler Dateien verwendet
Es ist statisch verknüpft, sodass Sie es nicht löschen können.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="209"/>
        <source>play files directly from zip files</source>
        <translation>Dateien direkt aus ZIP-Dateien abspielen</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="213"/>
        <source>Ogg Vorbis decoder using standard xiph.org libraries</source>
        <translation>Ogg Vorbis-Decoder mit Standard-xiph.org-Bibliotheken</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="217"/>
        <source>AY8910/12 chip emulator and vtx file player</source>
        <translation>AY8910/12 Chip-Emulator und vtx-Datei-Player</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="222"/>
        <source>WavPack (.wv, .iso.wv) player</source>
        <translation>WavPack (.wv und .iso.wv)-Player</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="226"/>
        <source>MIDI player based on WildMidi library

Requires freepats package to be installed
See http://freepats.zenvoid.org/
Make sure to set correct freepats.cfg path in plugin settings.</source>
        <translation>MIDI-Player basierend auf der WildMidi-Bibliothek.

Erfordert freepats.
Weitere Infos unter http://freepat.zenvoid.org/
Bitte stelle sicher in den Plugin-Einstellungen den richtigen freepats.cfg-Pfad zu setzen.</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="231"/>
        <source>plays WMA files</source>
        <translation>Spielt WMA-Dateien</translation>
    </message>
</context>
<context>
    <name>PluginSettings</name>
    <message>
        <location filename="../qml/dialogs/PluginSettings.qml" line="15"/>
        <source>%1 settings</source>
        <translation>%1-Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="108"/>
        <source>Enable scrobbler</source>
        <translation>Scrobbler aktivieren</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="109"/>
        <source>Disable nowplaying</source>
        <translation>NowPlaying deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="110"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="111"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="112"/>
        <source>Scrobble URL</source>
        <translation>Scrobble URL</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="113"/>
        <source>Prefer Album Artist over Artist field</source>
        <translation>Das Feld &quot;Album-Künstler&quot; gegenüber dem Feld &quot;Künstler&quot; bevorzugen</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="114"/>
        <source>Send MusicBrainz ID</source>
        <translation>MusicBrainz ID senden</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="115"/>
        <source>Submit tracks shorter than 30 seconds (not recommended)</source>
        <translation>Senden von Tracks, die kürzer als 30 Sekunden sind (nicht empfohlen)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="100"/>
        <source>Max song length (in minutes)</source>
        <translation>Maximale Track-Länge (in Minuten)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="101"/>
        <source>Fadeout length (seconds)</source>
        <translation>Fadeout-Länge (in Sekunden)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="102"/>
        <source>Play loops nr. of times (if available)</source>
        <translation>Schleifen n mal spielen (wenn verfügbar)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="155"/>
        <source>Preferred buffer size</source>
        <translation>Bevorzugte Buffer-Größe</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="167"/>
        <source>Relative seek table path</source>
        <translation>Relativer Such-Tabellenpfad</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="168"/>
        <source>Absolute seek table path</source>
        <translation>Absoluter Such-Tabellenpfad</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="169"/>
        <source>Swap audio bytes (toggle if all you hear is static)</source>
        <translation>Audiobytes tauschen (umschalten, wenn alles, was Sie hören, statisch ist)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="174"/>
        <source>Enable HVSC Songlength DB</source>
        <translation>HVSC Songlength DB aktivieren</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="161"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="176"/>
        <source>Samplerate</source>
        <translation>Samplerate</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="178"/>
        <source>Mono synth</source>
        <translation>Mono-Synth</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="179"/>
        <source>Default song length (sec)</source>
        <translation>Standard Track-Länge (in Sekunden)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="201"/>
        <source>Enable logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="227"/>
        <source>Timidity++ bank configuration file</source>
        <translation>Konfigurationsdatei für den Timidity ++ - Synthesizer</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="88"/>
        <source>Use all extensions supported by ffmpeg</source>
        <translation>Alle von ffmpeg unterstützten Erweiterungen benutzen</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="89"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="184"/>
        <source>File Extensions (separate with &apos;;&apos;)</source>
        <translation>Dateierweiterungen (getrennt mit &apos;;&apos;)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="132"/>
        <source>Backend</source>
        <translation>Backend</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="36"/>
        <source>Prefer Ken emu over Satoh (surround won&apos;t work)</source>
        <translation>Bevorzugen Sie Ken Emu gegenüber Satoh (Surround-Sound wird nicht funktionieren).</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="37"/>
        <source>Enable surround</source>
        <translation>Surround aktivieren</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="50"/>
        <source>Cache update period (in hours, 0=never)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="51"/>
        <source>Fetch from embedded tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="52"/>
        <source>Fetch from local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="53"/>
        <source>Fetch from Last.fm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="54"/>
        <source>Fetch from MusicBrainz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="55"/>
        <source>Fetch from Albumart.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="56"/>
        <source>Fetch from World of Spectrum (AY files only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="57"/>
        <source>When no artwork is found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="58"/>
        <source>Custom image path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="59"/>
        <source>Scale artwork towards longer side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="76"/>
        <source>Resampling quality</source>
        <translation>Abtast-Qualität</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="77"/>
        <source>8-bit output</source>
        <translation>8-Bit Ausgabe</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="78"/>
        <source>Internal DUMB volume</source>
        <translation>Interne DUMB-Lautstärke</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="79"/>
        <source>Volume ramping</source>
        <translation>Lautstärkeanhebung</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="94"/>
        <source>Ignore bad header errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="95"/>
        <source>Ignore unparsable stream errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="103"/>
        <source>ColecoVision BIOS (for SGC file format)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="133"/>
        <source>Force 16 bit output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="154"/>
        <source>PulseAudio server (leave empty for default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="160"/>
        <source>Default song length (in minutes)</source>
        <translation>Standard Track-Länge (in Minuten)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="162"/>
        <source>Skip when shorter than (sec)</source>
        <translation>Überspringen wenn kürzer als (in Sekunden)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="175"/>
        <source>Full path to Songlengths.md5/.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="177"/>
        <location filename="../qml/translation/ExtraStrings.qml" line="218"/>
        <source>Bits per sample</source>
        <translation>Bits pro Sample</translation>
    </message>
</context>
<context>
    <name>Plugins</name>
    <message>
        <location filename="../qml/dialogs/Plugins.qml" line="11"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="29"/>
        <source>Playback options</source>
        <translation>Wiedergabeeinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="33"/>
        <source>Looping</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="42"/>
        <source>Don&apos;t loop</source>
        <translation>Keine Wiederholung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="48"/>
        <source>Loop single song</source>
        <translation>Einzelnen Track wiederholen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="54"/>
        <source>Loop all</source>
        <translation>Alle wiederholen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="61"/>
        <source>Playback order</source>
        <translation>Wiedergabereihenfolge</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="71"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="77"/>
        <source>Shuffle tracks</source>
        <translation>Tracks mischen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="83"/>
        <source>Shuffle albums</source>
        <translation>Alben mischen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="89"/>
        <source>Random</source>
        <translation>Zufällig</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="97"/>
        <source>Resume previous session on startup</source>
        <translation>Beim Start zur letzten Sitzung zurückkehren</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="112"/>
        <source>Pause playback when headset is unplugged</source>
        <translation>Wiedergabe pausieren, wenn Headset getrennt wird</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="119"/>
        <source>Resume playback when headset is plugged</source>
        <translation>Wiedergabe fortsetzen, wenn Headset verbunden wird</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="126"/>
        <source>View options</source>
        <translation>Anzeigeeinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="139"/>
        <source>Playlist grouping</source>
        <translation>Wiedergabelistengruppierung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="149"/>
        <source>None</source>
        <translation>Ohne</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="155"/>
        <source>Artist/Date/Album</source>
        <translation>Künstler/Datum/Album</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="161"/>
        <source>Artist</source>
        <translation>Künstler</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="167"/>
        <source>Custom</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="182"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="197"/>
        <source>Please use &lt;a href=&quot;%1&quot;&gt;this&lt;/a&gt; spec as a reference on format fields for grouping</source>
        <translation>Bitte verwende &lt;a href=&quot;%1&quot;&gt;diese Spezifikation&lt;/a&gt; als Referenz für Formatfelder für die Gruppierung</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="130"/>
        <source>Use simple one-line playlist items</source>
        <translation>Einfache einzeilige Wiedergabelistenelemente verwenden</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="211"/>
        <source>Display album art for artist/album groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="218"/>
        <source>Display album art on cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="251"/>
        <source>Setup Last.fm</source>
        <translation>Last.fm einrichten</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/Settings.qml" line="261"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
</context>
<context>
    <name>TrackProperties</name>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="28"/>
        <source>Track Properties</source>
        <translation>Track-Eigenschaften</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="45"/>
        <source>Metadata</source>
        <translation>Metadaten</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="49"/>
        <source>No metadata</source>
        <translation>Keine Metadaten vorhanden</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="67"/>
        <source>Properties</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location filename="../qml/dialogs/TrackProperties.qml" line="71"/>
        <source>No properties</source>
        <translation>Keine Eigenschaften vorhanden</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="8"/>
        <source>Artist</source>
        <translation>Künstler</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="9"/>
        <source>Track Title</source>
        <translation>Track-Titel</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="10"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="11"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="12"/>
        <source>Track Number</source>
        <translation>Track-Nummer</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="13"/>
        <source>Total Tracks</source>
        <translation>Tracks insgesamt</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="14"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="15"/>
        <source>Composer</source>
        <translation>Komponist</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="16"/>
        <source>Disc Number</source>
        <translation>CD-Nummer</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="17"/>
        <source>Total Disks</source>
        <translation>CDs insgesamt</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="18"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="22"/>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="23"/>
        <source>Subtrack Index</source>
        <translation>Subtrack-Index</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="24"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="25"/>
        <source>Tag Type(s)</source>
        <translation>Tag-Typ(en)</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="26"/>
        <source>Embedded Cuesheet</source>
        <translation>Eingebettetes Cuesheet</translation>
    </message>
    <message>
        <location filename="../qml/translation/ExtraStrings.qml" line="27"/>
        <source>Codec</source>
        <translation>Codec</translation>
    </message>
</context>
</TS>
