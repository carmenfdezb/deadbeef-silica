import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property alias text: label.text
    property alias imagePath: image.source

    property bool displayCover: true

    width: parent.width
    height: (image.visible ? 29 : 0) + image.height + 29 + label.height + 29

    Image {
       id: image

       anchors.left: parent.left
       anchors.leftMargin: Theme.horizontalPageMargin
       anchors.right: parent.right
       anchors.rightMargin: Theme.horizontalPageMargin
       anchors.top: parent.top
       anchors.topMargin: visible ? 29 : 0

       visible: displayCover && (status === Image.Loading || status === Image.Ready)

       // WA: Always use square cover to get rid of ListView layout glitches
       height: visible ? width : 0
       fillMode: Image.PreserveAspectFit

       asynchronous: true
    }

    Label {
        id: label

        anchors.left: parent.left
        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.right: parent.right
        anchors.rightMargin: Theme.horizontalPageMargin
        anchors.top: image.bottom
        anchors.topMargin: 29

        horizontalAlignment: Text.AlignRight
        wrapMode: Text.Wrap

        font.pixelSize: Theme.fontSizeSmall
        color: Theme.highlightColor
    }
}
