import QtQuick 2.0
import Sailfish.Silica 1.0

// FIXME: Refactor FileSystemModel into separate module
import deadbeef 1.0

import "../items"

Dialog {
    property alias selectFiles: fileSystemModel.selectFiles
    property alias selectDirectories: fileSystemModel.selectDirectories

    property alias multiple: fileSystemModel.multiple
    property alias showHidden: fileSystemModel.showHidden

    property alias path: fileSystemModel.path

    property var resultPaths: []

    property alias filterNames: fileSystemModel.filterNames

    property alias title: dialogTitle.text

    function installFilter(name, functor) {
        return fileSystemModel.installFilter(name, functor);
    }

    function removeFilter(index) {
        fileSystemModel.removeFilter(index);
    }

    onDone: {
        if (fileSystemModel.selectedPaths.length !== 0)
        {
            resultPaths = fileSystemModel.selectedPaths;
        }
        else
        {
            resultPaths[0] = path;
        }
    }

    canAccept: fileSystemModel.selectedPaths.length !== 0 || selectDirectories

    FileSystemModel {
        id: fileSystemModel
        activeFilterIndex: filtersComboBox.currentIndex
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: fileSystemModel.showHidden ? qsTr("Hide system files") :
                                                   qsTr("Show system files")

                onClicked: fileSystemModel.showHidden = !fileSystemModel.showHidden
            }
        }

        DialogHeader {
            id: dialogHeader

            acceptText: selectDirectories && fileSystemModel.selectedPaths.length === 0 ? qsTr("Select current dir") :
                                                                                          qsTr("Select")
            spacing: Theme.paddingMedium    // to emulate interval between header and its native title
        }

        DialogTitle {
            id: dialogTitle
            anchors.top: dialogHeader.bottom
        }

        ComboBox {
            id: filtersComboBox

            visible: fileSystemModel.filterNames.length !== 0

            width: parent.width
            anchors.top: dialogTitle.bottom
            anchors.topMargin: Theme.paddingLarge

            label: qsTr("Filter")

            menu: ContextMenu {
                Repeater {
                    model: fileSystemModel.filterNames

                    delegate: MenuItem {
                        text: modelData
                    }
                }
            }
        }

        Label {
            id: pathLabel

            anchors.top: filtersComboBox.bottom
            anchors.left: parent.left
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.right: parent.right
            anchors.rightMargin: Theme.horizontalPageMargin

            clip: true

            text: qsTr("Path") + ": " + fileSystemModel.path

            color: Theme.highlightColor
        }

        SilicaListView {
            width: parent.width
            anchors.top: pathLabel.bottom
            anchors.topMargin: Theme.paddingMedium
            anchors.bottom: parent.bottom

            clip: true

            model: fileSystemModel

            delegate: ListItem {
                id: listItem

                width: parent.width

                Image {
                    id: icon
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.paddingSmall
                    anchors.verticalCenter: parent.verticalCenter
                    width: height
                    height: parent.height

                    source: "image://theme/icon-m-file-" + (is_dir ? "folder" : "document")
                    fillMode: Image.PreserveAspectFit
                }

                Label {
                    id: nameLabel

                    anchors.left: icon.right
                    anchors.leftMargin: Theme.paddingSmall
                    anchors.right: selectionSwitch.left
                    anchors.rightMargin: Theme.paddingSmall
                    anchors.verticalCenter: parent.verticalCenter

                    text: name
                    color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                    elide: Text.ElideMiddle
                }

                Switch {
                    id: selectionSwitch
                    anchors.right: parent.right
                    anchors.rightMargin: Theme.horizontalPageMargin
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: height

                    visible: checkable

                    automaticCheck: false
                    checked: model.checked

                    onClicked: {
                        if (checked)
                        {
                            fileSystemModel.setCheckedState(index, false);
                        }
                        else
                        {
                            fileSystemModel.setCheckedState(index, true);
                        }
                    }
                }

                onClicked: {
                    if (is_dir)
                    {
                        fileSystemModel.path = path;
                    }
                }
            }

            VerticalScrollDecorator {}
        }
    }
}
