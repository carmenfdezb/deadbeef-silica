#ifndef __STRUTILS_H
#define __STRUTILS_H

#include <QString>

QString PtrToQString(void *ptr);
void* QStringToPtr(QString str);

#endif // __STRUTILS_H
