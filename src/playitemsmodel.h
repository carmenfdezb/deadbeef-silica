#ifndef __PLAYITEMSMODEL_H
#define __PLAYITEMSMODEL_H

#include <deadbeef/deadbeef.h>
#include <deadbeef/artwork.h>

#include "abstractqmllistmodel.h"

#include <QMap>

class CPlayitemsModel: public CAbstractQmlListModel
{
    Q_OBJECT

    Q_PROPERTY(int nowPlayingIdx READ nowPlayingIdx NOTIFY nowPlayingIdxChanged)

    // FIXME: This looks weakly related to playlist model. Move to CDdbApi?
    Q_PROPERTY(QString nowPlayingtTitle READ nowPlayingTitle NOTIFY nowPlayingTitleChanged)
    Q_PROPERTY(QString nowPlayingArtistAlbum READ nowPlayingArtistAlbum NOTIFY nowPlayingArtistAlbumChanged)

    Q_PROPERTY(EGroupingMode groupingMode READ groupingMode WRITE setGroupingMode NOTIFY groupingModeChanged)
    Q_PROPERTY(QString customGroupingFormatString READ customGroupingFormatString WRITE setCustomGroupingFormatString NOTIFY customGroupingFormatStringChanged)

public:
    enum EPlayitemsRoles
    {
        FirstLineRole = Qt::UserRole,
        SecondLineRole,
        DurationRole,
        IsNowPlayingRole,
        IsInPlayqueueRole,
        GroupingKeyRole,
        CoverUriRole
    };

    enum ESortOrder
    {
        SortAscending,
        SortDescending,
        SortRandom
    };

    Q_ENUM(ESortOrder)

    enum EGroupingMode
    {
        GroupingNone,
        GroupingArtistYearAlbum,
        GroupingArtist,
        GroupingCustom
    };

    Q_ENUM(EGroupingMode)

    CPlayitemsModel(DB_functions_t *api);
    ~CPlayitemsModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    int nowPlayingIdx();
    QString nowPlayingTitle();
    QString nowPlayingArtistAlbum();

    EGroupingMode groupingMode();
    void setGroupingMode(EGroupingMode mode);

    QString customGroupingFormatString();
    void setCustomGroupingFormatString(QString formatString);

    Q_INVOKABLE void sortByTf(QString fmt, ESortOrder sortOrder);

    void setArtworkPlugin(DB_artwork_plugin_t *artworkPlugin);

    void onPlaylistChanged();
    void onPlayqueueChanged();
    void onTrackChanged(int newTrackIdx);

    void onConfigChanged();

signals:
    void nowPlayingIdxChanged();
    void nowPlayingTitleChanged();
    void nowPlayingArtistAlbumChanged();
    void groupingModeChanged();
    void customGroupingFormatStringChanged();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QString produceTFormattedStringForIndex(int itemIdx, const QString &format) const;
    QString produceTFormattedStringForPlayitem(DB_playItem_t *it, const QString &format) const;

    bool isInPlayqueue(int itemIdx) const;

    QString getGroupingFormatString() const;

    void updateGroupingMode();
    void updateCustomGroupingFormatString();
    void emitGroupingKeyRoleChanged();

    void UpdateCovers();
    void SetCover(int idx, QString coverPath);

    typedef struct
    {
        CPlayitemsModel *playitemsModel;
        ddb_playItem_t *it;
    } SCoverReadyCbData;

    static void OnCoverReadyCb(const char *fname, const char *artist, const char *album, void *userData);
    Q_INVOKABLE void OnCoverReadyCbSync(char *fname, char *artist, char *album, void *userData);

    DB_functions_t *m_api;
    int m_currentItemsCount;
    int m_currentTrackIdx;

    EGroupingMode m_groupingMode;
    QString m_customGroupingFormatString;

    DB_artwork_plugin_t *m_artworkPlugin;

    QString m_defaultArtworkPath;

    typedef QVector<QString> QStringVector;
    QStringVector m_artworkPaths;

    QMap<QString, QString> m_groupArtworkPaths;
};

#endif // __PLAYITEMSMODEL_H
