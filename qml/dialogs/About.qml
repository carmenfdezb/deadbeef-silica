import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    function openUrlExternallyNoTel(link) {
        // Forbid to activate tel. number
        if (link.length >= 3 && link.substring(0, 3) === "tel")
            return;

        Qt.openUrlExternally(link);
    }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: contentColumn.height + Theme.paddingLarge

        Column {
            id: contentColumn
            width: parent.width

            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("About")
            }

            SectionHeader {
                text: "DeadBeef-Silica"
            }

            LinkedLabel {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                plainText: qsTr("DeadBeef-Silica GUI plugin %1\n\
                                 Copyright © %2 Evgeny Kravchenko <cravchik@yandex.ru>\n\
                                 This program is licensed under the terms of GPLv3 license\n\
                                 \n\
                                 Please use https://bitbucket.org/kravich/deadbeef-silica/issues \
                                 tracker to file bug reports and feature requests.\n\
                                 \n\
                                 Credits:\n\
                                 \n\
                                 %3").arg(DdbApi.version).arg("2017-2021").arg(contributors)

                property string contributors: [qsTr("Vyacheslav Dikonov <sdiconov@mail.ru>\n\
                                                    Nikolai Sinyov\n\
                                                    Russian translation"),
                                               qsTr("Carmen Fdez. B.\n\
                                                    J. Lavoie\n\
                                                    Spanish translation"),
                                               qsTr("Allan Nordhøy\n\
                                                    Norwegian Bokmål translation"),
                                               qsTr("Quenti\n\
                                                    objectifnul\n\
                                                    ButterflyOfFire\n\
                                                    Nathan\n\
                                                    J. Lavoie\n\
                                                    French translation"),
                                               qsTr("Rui Kon\n\
                                                    Elizabeth Sherrock\n\
                                                    Chinese translation"),
                                               qsTr("Szabó G\n\
                                                    Hungarian translation"),
                                               qsTr("O S\n\
                                                    J. Lavoie\n\
                                                    German translation"),
                                               qsTr("Åke Engelbrektson\n\
                                                     Swedish translation"),
                                               qsTr("Reima Vesterinen\n\
                                                     Finnish translation"),
                                               qsTr("Milo Ivir\n\
                                                     Croatian translation")].join("\n\n")

                wrapMode: Text.Wrap
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeSmall

                defaultLinkActions: false
                onLinkActivated: openUrlExternallyNoTel(link)
            }

            SectionHeader {
                text: "DeadBeef"
            }

            LinkedLabel {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                function loadTextFile(url) {
                    var request = new XMLHttpRequest();
                    request.open("GET", url, false);
                    request.send(null);
                    return request.responseText;
                }

                plainText: loadTextFile("file:///usr/share/doc/deadbeef/about.txt")

                wrapMode: Text.Wrap
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeSmall

                defaultLinkActions: false
                onLinkActivated: openUrlExternallyNoTel(link)
            }

            SectionHeader {
                text: qsTr("DeadBeef License")
            }

            LinkedLabel {
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                }

                plainText: qsTr("DeadBeef is licensed under the terms of zlib license. \
For more information please refer to \
%1").arg("https://github.com/DeaDBeeF-Player/deadbeef/blob/master/COPYING")

                wrapMode: Text.Wrap
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeSmall
            }
        }

        VerticalScrollDecorator {}
    }
}
