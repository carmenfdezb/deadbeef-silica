import QtQuick 2.0
import Sailfish.Silica 1.0

import "../items"

Dialog {
    id: addLocationDialog

    canAccept: locationLabel.textValid

    property string location: locationLabel.text

    DialogHeader {
        id: dialogHeder
        spacing: Theme.paddingMedium    // to emulate interval between header and its native title
    }

    DialogTitle {
        id: dialogTitle
        anchors.top: dialogHeder.bottom
        text: qsTr("Add location")
    }

    Column {
        anchors {
            top: dialogTitle.bottom
            topMargin: Theme.paddingLarge
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        spacing: Theme.paddingSmall

        TextField {
            id: locationLabel

            width: parent.width

            focus: true
            label: qsTr("Location (URI or file path)")
            placeholderText: label
            inputMethodHints: Qt.ImhNoAutoUppercase

            property bool textValid: text.length !== 0

            EnterKey.iconSource: "image://theme/icon-m-enter-accept"
            EnterKey.onClicked: addLocationDialog.accept()
            EnterKey.enabled: textValid
        }
    }
}
