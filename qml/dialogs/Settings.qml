import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

Page {
    onStatusChanged: {
        if (status === PageStatus.Deactivating) {
            // Save custom format string when closing settings page
            DdbApi.playitemsModel.customGroupingFormatString = customGroupingFormatTextField.text;
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: settingsColumn.height + Theme.paddingLarge

        Column {
            id: settingsColumn

            width: parent.width

            PageHeader {
                title: qsTr("Settings")
            }

            SectionHeader {
                text: qsTr("Playback options")
            }

            ComboBox {
                label: qsTr("Looping")

                currentItem: DdbApi.playbackMode === DdbApi.LoopNone    ? loopNone :
                             DdbApi.playbackMode === DdbApi.LoopSingle  ? loopSingle :
                                                                          loopAll

                menu: ContextMenu {
                    MenuItem {
                        id: loopNone
                        text: qsTr("Don't loop")
                        onClicked: DdbApi.playbackMode = DdbApi.LoopNone
                    }

                    MenuItem {
                        id: loopSingle
                        text: qsTr("Loop single song")
                        onClicked: DdbApi.playbackMode = DdbApi.LoopSingle
                    }

                    MenuItem {
                        id: loopAll
                        text: qsTr("Loop all")
                        onClicked: DdbApi.playbackMode = DdbApi.LoopAll
                    }
                }
            }

            ComboBox {
                label: qsTr("Playback order")

                currentItem: DdbApi.playbackOrder === DdbApi.OrderLinear        ? orderLinear :
                             DdbApi.playbackOrder === DdbApi.OrderShuffleTracks ? orderShuffleTracks :
                             DdbApi.playbackOrder === DdbApi.OrderShuffleAlbums ? orderShuffleAlbums :
                                                                                  orderRandom

                menu: ContextMenu {
                    MenuItem {
                        id: orderLinear
                        text: qsTr("Linear")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderLinear
                    }

                    MenuItem {
                        id: orderShuffleTracks
                        text: qsTr("Shuffle tracks")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderShuffleTracks
                    }

                    MenuItem {
                        id: orderShuffleAlbums
                        text: qsTr("Shuffle albums")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderShuffleAlbums
                    }

                    MenuItem {
                        id: orderRandom
                        text: qsTr("Random")
                        onClicked: DdbApi.playbackOrder = DdbApi.OrderRandom
                    }

                }
            }

            TextSwitch {
                text: qsTr("Resume previous session on startup")

                property bool isApiVersionSupported: DdbApi.apiVersion.major === 1 &&
                                                     DdbApi.apiVersion.minor >= 10

                enabled: isApiVersionSupported
                visible: enabled

                checked: isApiVersionSupported ? DdbApi.confGetInt("resume_last_session", 1) : false
                onCheckedChanged: if (isApiVersionSupported) {
                                      DdbApi.confSetInt("resume_last_session", checked === true ? 1 : 0)
                                  }
            }

            TextSwitch {
                text: qsTr("Pause playback when headset is unplugged")

                checked: DdbApi.confGetInt("silica.playback.pause_on_headset_unplug", 1)
                onCheckedChanged: DdbApi.confSetInt("silica.playback.pause_on_headset_unplug", checked === true ? 1 : 0)
            }

            TextSwitch {
                text: qsTr("Resume playback when headset is plugged")

                checked: DdbApi.confGetInt("silica.playback.resume_on_headset_plug", 0)
                onCheckedChanged: DdbApi.confSetInt("silica.playback.resume_on_headset_plug", checked === true ? 1 : 0)
            }

            SectionHeader {
                text: qsTr("View options")
            }

            TextSwitch {
                text: qsTr("Use simple one-line playlist items")
                checked: DdbApi.useSingleLinePlaylistItems
                automaticCheck: false
                onClicked: DdbApi.useSingleLinePlaylistItems = !DdbApi.useSingleLinePlaylistItems
            }

            ComboBox {
                id: groupingComboBox

                label: qsTr("Playlist grouping")

                currentItem: DdbApi.playitemsModel.groupingMode === PlayitemsModel.GroupingArtistYearAlbum ? artistDateAlbumItem :
                             DdbApi.playitemsModel.groupingMode === PlayitemsModel.GroupingArtist          ? artistItem :
                             DdbApi.playitemsModel.groupingMode === PlayitemsModel.GroupingCustom          ? customItem :
                                                                                                             noneItem

                menu: ContextMenu {
                    MenuItem {
                        id: noneItem
                        text: qsTr("None")
                        onClicked: DdbApi.playitemsModel.groupingMode = PlayitemsModel.GroupingNone
                    }

                    MenuItem {
                        id: artistDateAlbumItem
                        text: qsTr("Artist/Date/Album")
                        onClicked: DdbApi.playitemsModel.groupingMode = PlayitemsModel.GroupingArtistYearAlbum
                    }

                    MenuItem {
                        id: artistItem
                        text: qsTr("Artist")
                        onClicked: DdbApi.playitemsModel.groupingMode = PlayitemsModel.GroupingArtist
                    }

                    MenuItem {
                        id: customItem
                        text: qsTr("Custom")
                        onClicked: DdbApi.playitemsModel.groupingMode = PlayitemsModel.GroupingCustom
                    }
                }
            }

            Column {
                width: parent.width

                enabled: visible
                visible: groupingComboBox.currentItem === customItem

                TextField {
                    id: customGroupingFormatTextField
                    width: parent.width
                    label: qsTr("Format")
                    placeholderText: label
                    text: DdbApi.playitemsModel.customGroupingFormatString
                }

                Label {
                    anchors {
                        left: parent.left
                        leftMargin: Theme.horizontalPageMargin
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }

                    wrapMode: Text.Wrap

                    text: qsTr("Please use <a href=\"%1\">this</a> spec as a reference on format fields for grouping")
                               .arg("http://wiki.hydrogenaud.io/index.php?title=Foobar2000:Title_Formatting_Reference")

                    color: Theme.highlightColor
                    linkColor: Theme.primaryColor

                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }

            TextSwitch {
                enabled: visible
                visible: groupingComboBox.currentItem === artistDateAlbumItem

                text: qsTr("Display album art for artist/album groups")
                checked: DdbApi.displayAlbumArtForGroups
                automaticCheck: false
                onClicked: DdbApi.displayAlbumArtForGroups = !DdbApi.displayAlbumArtForGroups
            }

            TextSwitch {
                text: qsTr("Display album art on cover")
                checked: DdbApi.displayAlbumArtOnCover
                automaticCheck: false
                onClicked: DdbApi.displayAlbumArtOnCover = !DdbApi.displayAlbumArtOnCover
            }

            Item {
                width: parent.width
                height: Theme.paddingLarge
            }

            ButtonLayout {
                Button {
                    function findPluginByName(pluginName) {
                        for (var i = 0; i < DdbApi.pluginsModel.count; i++) {
                            var plugin = DdbApi.pluginsModel.get(i);

                            if (plugin.name === pluginName)
                                return plugin;
                        }

                        return null;
                    }

                    property string lastFmPluginName: "last.fm scrobbler"

                    enabled: {
                        var plugin = findPluginByName(lastFmPluginName);
                        return plugin !== null && plugin.hasSettings;
                    }

                    visible: enabled

                    text: qsTr("Setup Last.fm")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginSettings.qml"),
                                              {
                                                  ptr: findPluginByName(lastFmPluginName).ptr
                                              })
                }

                Button {
                    ButtonLayout.newLine: true

                    text: qsTr("Plugins")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/Plugins.qml"))
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
