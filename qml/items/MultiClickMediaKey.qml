import QtQuick 2.0
import Sailfish.Media 1.0

MediaKey {
    signal singleClick
    signal doubleClick
    signal tripleClick

    property int _clicksCount

    Timer {
        id: clickTimer
        interval: 250

        onTriggered: {
            _clicksCount === 1 ? singleClick() :
            _clicksCount === 2 ? doubleClick() :
                                 tripleClick();  // >==3 for triple click for those who mash buttons at random :)
            _clicksCount = 0;
        }
    }

    onReleased: {
        clickTimer.restart();
        _clicksCount += 1;
    }
}
